webpackJsonp([15],{

/***/ 300:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssignmentPageModule", function() { return AssignmentPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__assignment__ = __webpack_require__(487);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AssignmentPageModule = /** @class */ (function () {
    function AssignmentPageModule() {
    }
    AssignmentPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__assignment__["a" /* AssignmentPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__assignment__["a" /* AssignmentPage */]),
            ],
        })
    ], AssignmentPageModule);
    return AssignmentPageModule;
}());

//# sourceMappingURL=assignment.module.js.map

/***/ }),

/***/ 487:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AssignmentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AssignmentPage = /** @class */ (function () {
    function AssignmentPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.listItems = [];
        this.index = 0;
    }
    AssignmentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AssignmentPage');
    };
    AssignmentPage.prototype.getData = function () {
        this.listItems.push({
            id: this.index + 1,
            title: "Homework",
            status: "pending",
            timestamp: new Date().toISOString()
        }, {
            id: this.index + 1,
            title: "Review",
            status: "pending",
            timestamp: new Date().toISOString()
        }, {
            id: this.index + 1,
            title: "Homework",
            status: "pending",
            timestamp: new Date().toISOString()
        });
    };
    AssignmentPage.prototype.details = function (item) {
        this.navCtrl.push('DetailsPage', {
            param: item
        });
    };
    AssignmentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-assignment',template:/*ion-inline-start:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\assignment\assignment.html"*/'<ion-header>\n\n\n\n  <ion-navbar color="customCol">\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-buttons end>\n\n      <ion-datetime placeholder="Select Month" displayFormat="MMMM" pickerFormat="MMMM" [(ngModel)]="selectedMonth"\n\n        (ionChange)="getData()">\n\n      </ion-datetime>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <div *ngIf="listItems.length > 0">\n\n    <ion-card *ngFor="let item of listItems" (click)="details(item)">\n\n\n\n      <ion-card-header style="font-size: 1.8em; padding: 5px;">\n\n        <ion-row>\n\n          <ion-col col-6>{{item.title}}</ion-col>\n\n          <ion-col col-6 style="text-align: right;">\n\n            <ion-badge style="font-size: 1.1em;" *ngIf="item.status !== \'pending\'" color="secondary">{{item.status}}\n\n            </ion-badge>\n\n            <ion-badge style="font-size: 1.1em;" *ngIf="item.status === \'pending\'" color="danger">{{item.status}}\n\n            </ion-badge>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-card-header>\n\n      <ion-card-content style="padding: 5px">\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <p style="font-size: 1.3em;">{{item.timestamp | date:\'short\'}}</p>\n\n          </ion-col>\n\n          <!-- <ion-col col-6 style="text-align: right;">\n\n            <p style="font-size: 1.3em;" *ngIf="item.status === \'Paid\'">{{item.amount}}</p>\n\n\n\n            <button ion-button *ngIf="item.status !== \'Paid\'" (click)="paynow()">Pay Now</button>\n\n          </ion-col> -->\n\n        </ion-row>\n\n      </ion-card-content>\n\n\n\n    </ion-card>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\assignment\assignment.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], AssignmentPage);
    return AssignmentPage;
}());

//# sourceMappingURL=assignment.js.map

/***/ })

});
//# sourceMappingURL=15.js.map