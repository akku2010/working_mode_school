webpackJsonp([9],{

/***/ 307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsPageModule", function() { return ReportsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__reports__ = __webpack_require__(496);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ReportsPageModule = /** @class */ (function () {
    function ReportsPageModule() {
    }
    ReportsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__reports__["a" /* ReportsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__reports__["a" /* ReportsPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], ReportsPageModule);
    return ReportsPageModule;
}());

//# sourceMappingURL=reports.module.js.map

/***/ }),

/***/ 496:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_caller_api_caller__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ReportsPage = /** @class */ (function () {
    function ReportsPage(navCtrl, navParams, toastCtrl, alertCtrl, apiCall) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.apiCall = apiCall;
        this.SelectVehicle = 'Select Student';
        this.mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        console.log("login details=> " + localStorage.getItem('details'));
        this.loginDetails = JSON.parse(localStorage.getItem('details'));
        console.log("login _id=> " + this.loginDetails._id);
        this.token = localStorage.getItem("DEVICE_TOKEN");
        this.deviceUUID = localStorage.getItem("UUID");
        console.log("UUID=> ", this.deviceUUID);
    }
    ReportsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReportsPage');
    };
    ReportsPage.prototype.showDetails = function (childdata) {
        var combinedData = {
            "statusReport": this.stuReport,
            "studentId": this.selectedStudent_id
        };
        this.navCtrl.push("DetailedReportPage", {
            data: combinedData,
            param: childdata,
            childName: this.selectedStudent
        });
    };
    ReportsPage.prototype.ngOnInit = function () {
        this.getStudent();
    };
    ReportsPage.prototype.getStudent = function () {
        var _this = this;
        var data = {
            "_find": { "parent": { "_eval": "Id", "value": this.loginDetails._id } }
        };
        this.apiCall.startLoading().present();
        this.apiCall.getStudent(data)
            .subscribe(function (data) {
            console.log("student data", data);
            _this.portstemp = data;
            _this.liststudent = [];
            for (var i = 0; i < data.length; i++) {
                _this.liststudent.push({ 'name': data[i].name.fname, 'id': data[i]._id, 'scheduleId': data[i].scheduledTrips });
            }
            console.log("student data name", _this.liststudent);
            _this.apiCall.stopLoading();
            localStorage.setItem('student', data);
            _this.isstudent = localStorage.getItem('student');
        }, function (error) {
            _this.apiCall.stopLoading();
            console.log(error);
        });
    };
    ReportsPage.prototype.onChangedSelect = function (selectedStudent) {
        var _this = this;
        console.log("selectedStudent=> ", selectedStudent);
        this.selectedStudent_id = selectedStudent.id;
        this.student_scheduleId = selectedStudent.scheduleId;
        this.selectedStudent = selectedStudent;
        console.log("selectedStudent id=> ", this.selectedStudent_id);
        var data = {
            "student": { "_eval": "Id", "value": this.selectedStudent_id },
            "scheduleSchool": { "_eval": "Id", "value": this.student_scheduleId }
        };
        this.apiCall.startLoading().present();
        this.apiCall.studentlistreportCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("student report data", data);
            var arr = data.total.filter(function (d) { return d._id; }).map(function (d) { return d._id; });
            var allRes = [];
            var _loop_1 = function (x) {
                var dd = {};
                dd.year = arr[x].year;
                dd.month = arr[x].month;
                temp = parseInt(arr[x].month);
                dd.totalTrip = data.total.filter(function (d) { return d._id.year == arr[x].year && d._id.month == arr[x].month; })[0].total;
                var totalPresent = data.student.filter(function (d) { return d._id.year == arr[x].year && d._id.month == arr[x].month; });
                if (totalPresent == undefined || totalPresent.length == 0) {
                    dd.totalPresent = 0;
                }
                else {
                    dd.totalPresent = totalPresent[0].total;
                }
                // dd.totalPresent = data.student.filter(function (d) { return d._id.year == arr[x].year && d._id.month == arr[x].month; })[0].total;
                // if(dd.totalPresent== undefined){
                //   dd.totalPresent='0';
                // }else{
                //   dd.totalPresent = data.student.filter(function (d) { return d._id.year == arr[x].year && d._id.month == arr[x].month; })[0].total;
                // }
                dd.monthString = _this.mL[temp - 1];
                dd.absent = (dd.totalTrip - dd.totalPresent);
                allRes.push(dd);
            };
            var temp;
            for (var x in arr) {
                _loop_1(x);
            }
            console.log(allRes);
            _this.stuReport = allRes;
            // console.log(this.studentReport[0].statusType);
            // for (var i = 0; i < this.studentReport.length; i++) {
            //   console.log("student data name",this.studentReport[i].data.statusType);
            //   this.datareport = [];
            //   this.datareport.push({datastatusType:this.studentReport[i].data.statusType});
            // }
            // console.log("student report data", this.datareport );
            localStorage.setItem('student', _this.stuReport);
            _this.isstudent = localStorage.getItem('student');
        }, function (error) {
            _this.apiCall.stopLoading();
            console.log(error);
        });
    };
    ReportsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-reports',template:/*ion-inline-start:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\reports\reports.html"*/'<ion-header>\n\n  <ion-navbar color="customCol">\n\n    <ion-title>Monthly Reports</ion-title>\n\n  </ion-navbar>\n\n  <ion-item>\n\n    <ion-label>{{SelectVehicle}}</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedStudent" [items]="liststudent" itemValueField="name" itemTextField="name"\n\n      [canSearch]="true" (onChange)="onChangedSelect(selectedStudent)">\n\n    </select-searchable>\n\n  </ion-item>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-card style="background-color: #c4cacf" (tap)="showDetails(childdata)" *ngFor="let childdata of stuReport">\n\n    <ion-card-header class="headClass">\n\n      <h1>{{childdata.monthString}}</h1>\n\n    </ion-card-header>\n\n    <ion-card-content>\n\n      <ion-row style="background-color: #faa11f">\n\n        <ion-col col-33 style="text-align: center;">\n\n          <h1>{{childdata.totalTrip}}</h1>\n\n        </ion-col>\n\n        <ion-col col-33 style="text-align: center;">\n\n          <h1>{{childdata.totalPresent}}</h1>\n\n        </ion-col>\n\n        <ion-col col-33 style="text-align: center;">\n\n          <h1>{{childdata.absent}}</h1>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row style="background-color: #ef4138">\n\n        <ion-col col-33 style="text-align: center;">\n\n          <h4>Trips</h4>\n\n        </ion-col>\n\n        <ion-col col-33 style="text-align: center;">\n\n          <h4>Present</h4>\n\n        </ion-col>\n\n        <ion-col col-33 style="text-align: center;">\n\n          <h4>Absent</h4>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-content>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\reports\reports.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_caller_api_caller__["a" /* ApiCallerProvider */]])
    ], ReportsPage);
    return ReportsPage;
}());

//# sourceMappingURL=reports.js.map

/***/ })

});
//# sourceMappingURL=9.js.map