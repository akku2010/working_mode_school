webpackJsonp([7],{

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultsPageModule", function() { return ResultsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__results__ = __webpack_require__(498);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ResultsPageModule = /** @class */ (function () {
    function ResultsPageModule() {
    }
    ResultsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__results__["a" /* ResultsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__results__["a" /* ResultsPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
            ],
        })
    ], ResultsPageModule);
    return ResultsPageModule;
}());

//# sourceMappingURL=results.module.js.map

/***/ }),

/***/ 498:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResultsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_caller_api_caller__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__ = __webpack_require__(208);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ResultsPage = /** @class */ (function () {
    function ResultsPage(navCtrl, navParams, apiCall, iab) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.iab = iab;
        this.liststudent = [];
        this.index = 0;
        this.resultList = [];
        this.loginDetails = JSON.parse(localStorage.getItem('details'));
    }
    ResultsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResultsPage');
    };
    ResultsPage.prototype.ngOnInit = function () {
        this.getStudent();
    };
    ResultsPage.prototype.getStudent = function () {
        var _this = this;
        var data = {
            "_find": { "parent": { "_eval": "Id", "value": this.loginDetails._id } }
        };
        this.apiCall.startLoading().present();
        this.apiCall.getStudent(data)
            .subscribe(function (data) {
            console.log("student data", data);
            // this.portstemp = data;
            _this.liststudent = [];
            for (var i = 0; i < data.length; i++) {
                _this.liststudent.push({ 'name': data[i].name.fname, 'id': data[i]._id, 'scheduleId': data[i].scheduledTrips });
            }
            console.log("student data name", _this.liststudent);
            _this.apiCall.stopLoading();
            localStorage.setItem('student', data);
            _this.isstudent = localStorage.getItem('student');
        }, function (error) {
            _this.apiCall.stopLoading();
            console.log(error);
        });
    };
    ResultsPage.prototype.onChangedSelect = function (selectedStudent) {
        var _this = this;
        console.log("selected student: ", selectedStudent);
        var url = this.apiCall.link + "users/getResult?student=" + selectedStudent.id;
        this.apiCall.startLoading().present();
        this.apiCall.getUrl(url)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("response data: ", data);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
        this.resultList = [];
        this.resultList.push({
            id: this.index + 1,
            title: "Half Yearly",
        }, {
            id: this.index + 1,
            title: "Yearly",
        }, {
            id: this.index + 1,
            title: "Monthly",
        }, {
            id: this.index + 1,
            title: "Quarterly",
        }, {
            id: this.index + 1,
            title: "Half Quarterly",
        });
    };
    ResultsPage.prototype.gotoNext = function () {
        var browser = this.iab.create('http://www.africau.edu/images/default/sample.pdf', '_system');
        browser.on('loadstop').subscribe(function (event) {
            browser.insertCSS({ code: "body{color: red;" });
        });
    };
    ResultsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-results',template:/*ion-inline-start:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\results\results.html"*/'<ion-header>\n\n  <ion-navbar color="customCol">\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Results</ion-title>\n\n    <ion-buttons end>\n\n      <select-searchable style="max-width: 100%;" item-content placeholder="Select Student"\n\n        [(ngModel)]="selectedStudent" [items]="liststudent" itemValueField="name" itemTextField="name"\n\n        [canSearch]="true" (onChange)="onChangedSelect(selectedStudent)">\n\n      </select-searchable>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <div *ngIf="resultList.length > 0">\n\n    <ion-card *ngFor="let item of resultList" (click)="gotoNext(item)">\n\n      <ion-card-header>\n\n        <ion-card-title>{{item.title}}</ion-card-title>\n\n      </ion-card-header>\n\n    </ion-card>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\results\results.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_caller_api_caller__["a" /* ApiCallerProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], ResultsPage);
    return ResultsPage;
}());

//# sourceMappingURL=results.js.map

/***/ })

});
//# sourceMappingURL=7.js.map