webpackJsonp([13],{

/***/ 302:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeePageModule", function() { return FeePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fee__ = __webpack_require__(489);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { SelectSearchableModule } from 'ionic-select-searchable';
var FeePageModule = /** @class */ (function () {
    function FeePageModule() {
    }
    FeePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fee__["a" /* FeePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__fee__["a" /* FeePage */]),
            ],
        })
    ], FeePageModule);
    return FeePageModule;
}());

//# sourceMappingURL=fee.module.js.map

/***/ }),

/***/ 489:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_caller_api_caller__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FeePage = /** @class */ (function () {
    function FeePage(navCtrl, navParams, apiCall) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.index = 0;
        this.pendingList = [
            {
                id: this.index + 1,
                title: "January",
                date: new Date().toISOString(),
                status: "Paid",
                amount: "1200"
            },
            {
                id: this.index + 1,
                title: "February",
                date: new Date().toISOString(),
                status: "Paid",
                amount: "1200"
            },
            {
                id: this.index + 1,
                title: "March",
                date: new Date().toISOString(),
                status: "Paid",
                amount: "1200"
            },
            {
                id: this.index + 1,
                title: "April",
                date: null,
                status: "Pending",
                amount: null
            }
        ];
        this.liststudent = [];
        this.razor_key = 'rzp_live_jB4onRx1BUUvxt';
        this.paymentAmount = 120000;
        this.currency = 'INR';
        this.islogin = JSON.parse(localStorage.getItem('details'));
    }
    FeePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FeePage');
        this.onChangedSelect();
    };
    FeePage.prototype.paynow = function (item) {
        // payWithRazor() {
        var options = {
            description: 'Credits towards consultation',
            image: 'https://i.imgur.com/GO0jiDP.jpg',
            currency: this.currency,
            key: this.razor_key,
            amount: item.amount + "00",
            name: "xyz",
            prefill: {
                email: this.islogin.email,
                contact: this.islogin.phn,
                name: this.islogin.fn + ' ' + this.islogin.ln
            },
            theme: {
                color: '#ef4138'
            },
            modal: {
                ondismiss: function () {
                    console.log('dismissed');
                }
            }
        };
        var successCallback = function (payment_id) {
            alert('payment_id: ' + payment_id);
            this.updateExpDate();
        };
        var cancelCallback = function (error) {
            alert(error.description + ' (Error ' + error.code + ')');
        };
        RazorpayCheckout.open(options, successCallback, cancelCallback);
        // }
    };
    // getStudent() {
    //   var data =
    //   {
    //     "_find": { "parent": { "_eval": "Id", "value": this.islogin._id } }
    //   }
    //   this.apiCall.startLoading().present();
    //   this.apiCall.getStudent(data)
    //     .subscribe(data => {
    //       console.log("student data", data);
    //       // this.portstemp = data;
    //       this.liststudent = [];
    //       for (var i = 0; i < data.length; i++) {
    //         this.liststudent.push({ 'name': data[i].name.fname, 'id': data[i]._id, 'scheduleId': data[i].scheduledTrips })
    //       }
    //       console.log("student data name", this.liststudent);
    //       this.apiCall.stopLoading();
    //       localStorage.setItem('student', data);
    //       // this.isstudent = localStorage.getItem('student');
    //     },
    //       error => {
    //         this.apiCall.stopLoading();
    //         console.log(error);
    //       });
    // }
    FeePage.prototype.onChangedSelect = function () {
        var _this = this;
        // console.log("selected student: ", selectedStudent);
        this.pendingList = [];
        var url = this.apiCall.link + "users/getTransportFee?parent=" + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.getUrl(url)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("response data: ", data);
            _this.pendingList = data;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    FeePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fee',template:/*ion-inline-start:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\fee\fee.html"*/'<ion-header>\n\n  <ion-navbar color="customCol">\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Transport Fee</ion-title>\n\n    <!-- <ion-buttons end>\n\n      <select-searchable style="max-width: 100%;" item-content placeholder="Select Student"\n\n        [(ngModel)]="selectedStudent" [items]="liststudent" itemValueField="name" itemTextField="name"\n\n        [canSearch]="true" (onChange)="onChangedSelect(selectedStudent)">\n\n      </select-searchable>\n\n    </ion-buttons> -->\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-card *ngFor="let item of pendingList">\n\n\n\n    <ion-card-header style="font-size: 1.8em; padding: 5px;">\n\n      <ion-row>\n\n        <ion-col col-6>{{item.year}}</ion-col>\n\n        <ion-col col-6 style="text-align: right;">\n\n          <ion-badge style="font-size: 1.1em;" *ngIf="item.Status === \'Paid\'" color="secondary">{{item.Status}}\n\n          </ion-badge>\n\n          <ion-badge style="font-size: 1.1em;" *ngIf="item.Status !== \'Paid\'" color="danger">{{item.Status}}</ion-badge>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-header>\n\n    <ion-card-content style="padding: 5px">\n\n      <ion-row>\n\n        <ion-col col-6>\n\n          <p style="font-size: 1.3em;" *ngIf="item.Status === \'Paid\'">{{item.paid_Date | date:\'short\'}}</p>\n\n        </ion-col>\n\n        <ion-col col-6 style="text-align: right;">\n\n          <p style="font-size: 1.3em;" *ngIf="item.Status === \'Paid\'">{{item.amount}}</p>\n\n          \n\n          <button ion-button *ngIf="item.Status !== \'Paid\'" (click)="paynow(item)">Pay Now</button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-content>\n\n\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\fee\fee.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_caller_api_caller__["a" /* ApiCallerProvider */]])
    ], FeePage);
    return FeePage;
}());

//# sourceMappingURL=fee.js.map

/***/ })

});
//# sourceMappingURL=13.js.map