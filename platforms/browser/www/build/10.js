webpackJsonp([10],{

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile__ = __webpack_require__(494);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfilePageModule = /** @class */ (function () {
    function ProfilePageModule() {
    }
    ProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */]),
            ],
        })
    ], ProfilePageModule);
    return ProfilePageModule;
}());

//# sourceMappingURL=profile.module.js.map

/***/ }),

/***/ 494:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_caller_api_caller__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, navParams, apicall, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicall = apicall;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.profilesStudent = [];
        this.allData = {};
        console.log("login details=> " + localStorage.getItem('details'));
        this.loginDetails = JSON.parse(localStorage.getItem('details'));
        console.log("login _id=> " + this.loginDetails._id);
        this.token = localStorage.getItem("DEVICE_TOKEN");
        this.deviceUUID = localStorage.getItem("UUID");
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        this.getProfile();
    };
    ProfilePage.prototype.ngOnInit = function () { };
    ProfilePage.prototype.getProfile = function () {
        var _this = this;
        this.apicall.startLoading().present();
        this.apicall.getprofileApi(this.loginDetails._id)
            .subscribe(function (data) {
            _this.apicall.stopLoading();
            console.log("data of data: ", data);
            // this.profilesStudent = data[0].data;
            _this.profilesParentname = data[0]._id.first_name;
            _this.profilesParentphone = data[0]._id.phone;
            _this.profilesParentemail = data[0]._id.email;
            _this.profilesParenterelation = data[0]._id.relation;
            _this.profilesParent = data[0]._id;
            var i = 0, howManyTimes = data[0].data.length;
            var that = _this;
            function f() {
                that.profilesStudent.push({
                    "fname": data[0].data[i].students.name.fname ? data[0].data[i].students.name.fname : 'N/A',
                    "schoolName": data[0].data[i].students.school.schoolName ? data[0].data[i].students.school.schoolName : 'N/A',
                    "saddress": data[0].data[i].students.school.address ? data[0].data[i].students.school.address : 'N/A',
                    "className": data[0].data[i].students.class.className ? data[0].data[i].students.class.className : 'N/A',
                    "gender": data[0].data[i].students.gender ? data[0].data[i].students.gender : 'N/A',
                    "bg": data[0].data[i].students.bg ? data[0].data[i].students.bg : 'N/A',
                    "haddress": data[0].data[i].students.address ? data[0].data[i].students.address : 'N/A',
                });
                var _studId = data[0].data[i].students._id;
                that.profilesStudent[that.profilesStudent.length - 1].studmapId = "b" + i;
                that.allData.map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* GoogleMaps */].create(that.profilesStudent[that.profilesStudent.length - 1].studmapId, {
                    camera: {
                        target: { lat: data[0].data[i].students.latLng.lat, lng: data[0].data[i].students.latLng.lng },
                        zoom: 13
                    }
                });
                that.allData.map.addMarker({
                    title: 'Student position',
                    icon: 'green',
                    animation: 'DROP',
                    draggable: true,
                    position: {
                        lat: data[0].data[i].students.latLng.lat,
                        lng: data[0].data[i].students.latLng.lng
                    }
                })
                    .then(function (marker) {
                    marker.on(__WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_DRAG_END, that.profilesStudent[that.profilesStudent.length - 1].studmapId)
                        .subscribe(function () {
                        that.markerlatlong = marker.getPosition();
                        __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["a" /* Geocoder */].geocode({
                            "position": {
                                lat: that.markerlatlong.lat,
                                lng: that.markerlatlong.lng
                            }
                        }).then(function (results) {
                            if (results.length == 0) {
                                return null;
                            }
                            var address = [
                                results[0].subThoroughfare || "",
                                results[0].thoroughfare || "",
                                results[0].locality || "",
                                results[0].adminArea || "",
                                results[0].postalCode || "",
                                results[0].country || ""
                            ].join(", ");
                            that.addressofstudent = results[0].extra.lines[0];
                            that.sendAddress(that.addressofstudent, _studId, that.markerlatlong.lat, that.markerlatlong.lng);
                        });
                    });
                });
                that.profilesStudent[that.profilesStudent.length - 1].mapid = "a" + i;
                that.allData.map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* GoogleMaps */].create(that.profilesStudent[that.profilesStudent.length - 1].mapid, {
                    camera: {
                        target: { lat: data[0].data[i].students.school.latLng.lat, lng: data[0].data[i].students.school.latLng.lng },
                        zoom: 12
                    }
                });
                that.allData.map.addMarker({
                    position: {
                        lat: data[0].data[i].students.school.latLng.lat,
                        lng: data[0].data[i].students.school.latLng.lng
                    }
                });
                i++;
                if (i < howManyTimes) {
                    setTimeout(f, 200);
                }
            }
            f();
            console.log("profile data: ", that.profilesStudent);
            if (_this.profilesStudent.length == 0) {
                var alert_1 = _this.alertCtrl.create({
                    message: "No Data Found",
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            _this.apicall.stopLoading();
            console.log(error);
        });
    };
    ProfilePage.prototype.sendAddress = function (address, ID, Lat, Long) {
        var _this = this;
        var stdn = {
            "_id": ID,
            "latLng": { "lat": Lat, "lng": Long },
            "address": address
        };
        this.apicall.startLoading().present();
        this.apicall.studenEdit(stdn)
            .subscribe(function (data) {
            _this.apicall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: 'Student location updated. !',
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
            // this.getProfile();
        }, function (error) {
            _this.apicall.stopLoading();
            console.log(error);
        });
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\profile\profile.html"*/'<ion-header>\n\n  <ion-navbar color="customCol">\n\n    <ion-title>Profile</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-card *ngFor="let dataofstudent of profilesStudent">\n\n    <div align="center" style="margin-top: 20%;">\n\n      <img class="profile-image" src="assets/imgs/dummy_user.jpg">\n\n      <h6 style="font-size: 2.4rem;">{{dataofstudent.fname}}</h6>\n\n    </div>\n\n\n\n    <ion-row style="padding:2%;border-bottom: 1px solid lightgrey;">\n\n      <ion-col col-1 style="text-align: center">\n\n        <ion-icon color="gray" name="school"></ion-icon>\n\n      </ion-col>\n\n      <ion-col col-4 style="font-size: 16px;color: gray;">School</ion-col>\n\n      <ion-col col-1>:</ion-col>\n\n      <ion-col col-6 style="text-align: left;font-size: 18px;color:black;">{{dataofstudent.schoolName}}</ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row style="padding:2%;">\n\n      <ion-col col-1 style="text-align: center">\n\n        <ion-icon name="pin" color="gray"></ion-icon>\n\n      </ion-col>\n\n      <ion-col col-4 style="font-size: 16px;color: gray;">School Add</ion-col>\n\n      <ion-col col-1>:</ion-col>\n\n      <ion-col col-6 style="text-align: left;font-size: 18px;color:black;">{{dataofstudent.saddress}}</ion-col>\n\n    </ion-row>\n\n    <h6 style="padding:4%;">SCHOOL LOCATION</h6>\n\n    <div id="{{dataofstudent.mapid}}" style="height: 200px;width: 100%;"></div>\n\n    <ion-row style="padding:2%; border-bottom: 1px solid lightgrey;">\n\n      <ion-col col-1 style="text-align: center">\n\n        <ion-icon name="pin" color="gray"></ion-icon>\n\n      </ion-col>\n\n      <ion-col col-4 style="font-size: 16px;color: gray;">Class</ion-col>\n\n      <ion-col col-1>:</ion-col>\n\n      <ion-col col-6 style="text-align: left;font-size: 18px;color:black;">{{dataofstudent.className}}</ion-col>\n\n    </ion-row>\n\n    <ion-row style="padding:2%; border-bottom: 1px solid lightgrey;">\n\n      <ion-col col-1 style="text-align: center">\n\n        <ion-icon name="people" color="gray"></ion-icon>\n\n      </ion-col>\n\n      <ion-col col-4 style="font-size: 16px;color: gray;">Gender</ion-col>\n\n      <ion-col col-1>:</ion-col>\n\n      <ion-col col-6 style="text-align: left;font-size: 18px;color:black;">{{dataofstudent.gender}}</ion-col>\n\n    </ion-row>\n\n    <ion-row style="padding:2%;border-bottom: 1px solid lightgrey;">\n\n      <ion-col col-1 style="text-align: center">\n\n        <ion-icon name="water" color="gray"></ion-icon>\n\n      </ion-col>\n\n      <ion-col col-4 style="font-size: 16px;color: gray;">Blood Group</ion-col>\n\n      <ion-col col-1>:</ion-col>\n\n      <ion-col col-6 style="text-align: left;font-size: 18px;color:black;">{{dataofstudent.bg}}</ion-col>\n\n    </ion-row>\n\n    <ion-row style="padding:2%;">\n\n      <ion-col col-1>\n\n        <ion-icon name="home" color="gray"></ion-icon>\n\n      </ion-col>\n\n      <ion-col col-4 style="font-size: 16px;color: gray;">Home Add</ion-col>\n\n      <ion-col col-1>:</ion-col>\n\n      <ion-col col-6 style="text-align: left;font-size: 18px;color:black;">{{dataofstudent.haddress}}</ion-col>\n\n    </ion-row>\n\n\n\n    <h6 style="padding:4%; ">PICKUP LOCATION</h6>\n\n    <ion-row style="padding:2%;">\n\n      <div id="{{dataofstudent.studmapId}}" style="height: 200px;width: 100%;"></div>\n\n    </ion-row>\n\n  </ion-card>\n\n  <ion-card>\n\n    <ion-card-header>Guardian</ion-card-header>\n\n    <ion-row style="margin:2%;border-bottom: 1px solid lightgrey;">\n\n      <ion-col col-4 style="font-size: 16px;color: gray;">\n\n        Name\n\n      </ion-col>\n\n      <ion-col col-1>:</ion-col>\n\n      <ion-col col-7 style="text-align: left;font-size: 18px;color:black;">{{profilesParentname}}</ion-col>\n\n    </ion-row>\n\n    <ion-row style="margin:2%;border-bottom: 1px solid lightgrey;">\n\n      <ion-col col-4 style="font-size: 16px;color: gray;">\n\n        Relation\n\n      </ion-col>\n\n      <ion-col col-1>:</ion-col>\n\n      <ion-col col-7 style="text-align: left;font-size: 18px;color:black;" *ngIf="profilesParenterelation != undefined">\n\n        {{profilesParenterelation}}</ion-col>\n\n      <ion-col col-7 style="text-align: left;font-size: 18px;color:black;" *ngIf="profilesParenterelation == undefined">\n\n        N/A</ion-col>\n\n    </ion-row>\n\n    <ion-row style="margin:2%;border-bottom: 1px solid lightgrey;">\n\n      <ion-col col-4 style="font-size: 16px;color: gray;">\n\n        Phone\n\n      </ion-col>\n\n      <ion-col col-1>:</ion-col>\n\n      <ion-col col-7 style="text-align: left;font-size: 18px;color:black;">{{profilesParentphone}}</ion-col>\n\n    </ion-row>\n\n    <ion-row style="margin:2%;">\n\n      <ion-col col-4 style="font-size: 16px;color: gray;">\n\n        Email\n\n      </ion-col>\n\n      <ion-col col-1>:</ion-col>\n\n      <ion-col col-7 style="text-align: left;font-size: 18px;color:black;">{{profilesParentemail}}</ion-col>\n\n    </ion-row>\n\n  </ion-card>\n\n  <ion-card>\n\n    <ion-card-header>\n\n      Emergency Contacts\n\n    </ion-card-header>\n\n    <ion-card-content no-padding>\n\n      <ion-row style="margin:2%;border-bottom: 1px solid lightgrey;">\n\n        <ion-col col-5 style="font-size: 16px;color: gray;">\n\n          Transport Admin\n\n        </ion-col>\n\n        <ion-col col-1>:</ion-col>\n\n        <ion-col col-6 style="text-align: left;font-size: 18px;color:black;">+91 563789999</ion-col>\n\n      </ion-row>\n\n      <ion-row style="margin:2%;border-bottom: 1px solid lightgrey;">\n\n        <ion-col col-5 style="font-size: 16px;color: gray;">\n\n          School Admin\n\n        </ion-col>\n\n        <ion-col col-1>:</ion-col>\n\n        <ion-col col-6 style="text-align: left;font-size: 18px;color:black;">+91 1212121212</ion-col>\n\n      </ion-row>\n\n      <ion-row style="margin:2%;border-bottom: 1px solid lightgrey;">\n\n        <ion-col col-5 style="font-size: 16px;color: gray;">\n\n          Police Station\n\n        </ion-col>\n\n        <ion-col col-1>:</ion-col>\n\n        <ion-col col-6 style="text-align: left;font-size: 18px;color:black;">+91 2323232323</ion-col>\n\n      </ion-row>\n\n      <ion-row style="margin:2%;">\n\n        <ion-col col-5 style="font-size: 16px;color: gray;">\n\n          Hospital\n\n        </ion-col>\n\n        <ion-col col-1>:</ion-col>\n\n        <ion-col col-6 style="text-align: left;font-size: 18px;color:black;">+91 4545454545</ion-col>\n\n      </ion-row>\n\n      <ion-row style="margin:2%;">\n\n        <ion-col col-5 style="font-size: 16px;color: gray;">\n\n          Ambulance\n\n        </ion-col>\n\n        <ion-col col-1>:</ion-col>\n\n        <ion-col col-6 style="text-align: left;font-size: 18px;color:black;">+91 7878787878</ion-col>\n\n      </ion-row>\n\n      <ion-row style="margin:2%;">\n\n        <ion-col col-5 style="font-size: 16px;color: gray;">\n\n          Fire Briged\n\n        </ion-col>\n\n        <ion-col col-1>:</ion-col>\n\n        <ion-col col-6 style="text-align: left;font-size: 18px;color:black;">+91 9090909676</ion-col>\n\n      </ion-row>\n\n    </ion-card-content>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_caller_api_caller__["a" /* ApiCallerProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ })

});
//# sourceMappingURL=10.js.map