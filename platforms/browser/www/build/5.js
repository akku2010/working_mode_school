webpackJsonp([5],{

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function() { return SettingsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings__ = __webpack_require__(499);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number__ = __webpack_require__(486);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SettingsPageModule = /** @class */ (function () {
    function SettingsPageModule() {
    }
    SettingsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__settings__["a" /* SettingsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__settings__["a" /* SettingsPage */]),
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number__["a" /* CallNumber */]]
        })
    ], SettingsPageModule);
    return SettingsPageModule;
}());

//# sourceMappingURL=settings.module.js.map

/***/ }),

/***/ 486:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CallNumber; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_core__ = __webpack_require__(12);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @name Call Number
 * @description
 * Call a number directly from your Cordova/Ionic application.
 * **NOTE**: The iOS Simulator (and maybe Android Simulators) do not provide access to the phone subsystem.
 *
 * @usage
 * ```typescript
 * import { CallNumber } from '@ionic-native/call-number';
 *
 * constructor(private callNumber: CallNumber) { }
 *
 * ...
 *
 *
 * this.callNumber.callNumber("18001010101", true)
 *   .then(res => console.log('Launched dialer!', res))
 *   .catch(err => console.log('Error launching dialer', err));
 *
 * ```
 */
var CallNumber = (function (_super) {
    __extends(CallNumber, _super);
    function CallNumber() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Calls a phone number
     * @param {string} numberToCall The phone number to call as a string
     * @param {boolean} bypassAppChooser Set to true to bypass the app chooser and go directly to dialer
     * @return {Promise<any>}
     */
    /**
       * Calls a phone number
       * @param {string} numberToCall The phone number to call as a string
       * @param {boolean} bypassAppChooser Set to true to bypass the app chooser and go directly to dialer
       * @return {Promise<any>}
       */
    CallNumber.prototype.callNumber = /**
       * Calls a phone number
       * @param {string} numberToCall The phone number to call as a string
       * @param {boolean} bypassAppChooser Set to true to bypass the app chooser and go directly to dialer
       * @return {Promise<any>}
       */
    function (numberToCall, bypassAppChooser) {
        return;
    };
    /**
     * Check if call feature is available
     * @return {Promise<any>}
     */
    /**
       * Check if call feature is available
       * @return {Promise<any>}
       */
    CallNumber.prototype.isCallSupported = /**
       * Check if call feature is available
       * @return {Promise<any>}
       */
    function () {
        return;
    };
    CallNumber.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])({
            callbackOrder: 'reverse'
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Boolean]),
        __metadata("design:returntype", Promise)
    ], CallNumber.prototype, "callNumber", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], CallNumber.prototype, "isCallSupported", null);
    /**
     * @name Call Number
     * @description
     * Call a number directly from your Cordova/Ionic application.
     * **NOTE**: The iOS Simulator (and maybe Android Simulators) do not provide access to the phone subsystem.
     *
     * @usage
     * ```typescript
     * import { CallNumber } from '@ionic-native/call-number';
     *
     * constructor(private callNumber: CallNumber) { }
     *
     * ...
     *
     *
     * this.callNumber.callNumber("18001010101", true)
     *   .then(res => console.log('Launched dialer!', res))
     *   .catch(err => console.log('Error launching dialer', err));
     *
     * ```
     */
    CallNumber = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["h" /* Plugin */])({
            pluginName: 'CallNumber',
            plugin: 'call-number',
            pluginRef: 'plugins.CallNumber',
            repo: 'https://github.com/Rohfosho/CordovaCallNumberPlugin',
            platforms: ['Android', 'iOS']
        })
    ], CallNumber);
    return CallNumber;
}(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["g" /* IonicNativePlugin */]));

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 499:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_app_version__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_caller_api_caller__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_call_number__ = __webpack_require__(486);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SettingsPage = /** @class */ (function () {
    function SettingsPage(navCtrl, navParams, alertCtrl, _app, appVersion, apiCall, storage, callNumber, plt) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this._app = _app;
        this.appVersion = appVersion;
        this.apiCall = apiCall;
        this.storage = storage;
        this.callNumber = callNumber;
        this.plt = plt;
        this.userdetails = JSON.parse(localStorage.getItem('details'));
        if (localStorage.getItem("NOTIF") != null) {
            this.notif = JSON.parse(localStorage.getItem("NOTIF"));
        }
        this.appVersion.getVersionNumber().then(function (version) {
            _this.aVer = version;
            console.log("app version=> " + _this.aVer);
        });
        this.person = { name: undefined, company: undefined, birthdate: undefined };
        this.dob = undefined;
    }
    SettingsPage.prototype.ionViewDidLoad = function () {
        var person = JSON.parse(localStorage.getItem('PERSON'));
        if (person) {
            this.person = person;
            this.age = this.getAge(this.person.birthdate);
            this.dob = new Date(this.person.birthdate).toISOString();
        }
    };
    SettingsPage.prototype.logout = function () {
        var _this = this;
        this.token = localStorage.getItem("DEVICE_TOKEN");
        var pushdata = {
            "uid": this.userdetails._id,
            "token": this.token,
            "os": "android",
            "app": "saftey365"
        };
        var alert = this.alertCtrl.create({
            message: 'Do you want to logout from the app?',
            buttons: [{
                    text: 'YES',
                    handler: function () {
                        _this.apiCall.startLoading().present();
                        _this.apiCall.pullnotifyCall(pushdata)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            console.log("push notifications updated " + data.message);
                            localStorage.clear();
                            _this.storage.remove("DEVICE_TOKEN");
                            _this._app.getRootNav().setRoot('LoginPage');
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            console.log(err);
                        });
                    }
                },
                {
                    text: 'No'
                }]
        });
        alert.present();
    };
    SettingsPage.prototype.notifChange = function (value) {
        var _this = this;
        if (value == false) {
            var alert_1 = this.alertCtrl.create({
                message: 'Do you want to stop getting notifications?',
                buttons: [{
                        text: 'Yes',
                        handler: function () {
                            _this.notif = value;
                            localStorage.setItem("NOTIF", JSON.stringify(_this.notif));
                        }
                    },
                    {
                        text: 'Back'
                    }]
            });
            alert_1.present();
        }
        else {
            localStorage.setItem("NOTIF", JSON.stringify(value));
        }
    };
    SettingsPage.prototype.reset = function () {
        this.person = { name: null, company: null, birthdate: null };
        this.dob = null;
        this.showProfile = false;
    };
    SettingsPage.prototype.save = function () {
        this.person.birthdate = new Date(this.dob).getTime();
        this.age = this.getAge(this.person.birthdate);
        this.showProfile = true;
        localStorage.setItem('PERSON', JSON.stringify(this.person));
    };
    SettingsPage.prototype.getAge = function (birthdate) {
        var currentTime = new Date().getTime();
        return ((currentTime - birthdate) / 31556952000).toFixed(0);
    };
    SettingsPage.prototype.call = function (num) {
        if (this.plt.is('android')) {
            this.callNumber.callNumber(num, true)
                .then(function (res) { return console.log('Launched dialer!', res); })
                .catch(function (err) { return console.log('Error launching dialer', err); });
        }
        else if (this.plt.is('ios')) {
            window.open('tel:' + num, '_system');
        }
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-settings',template:/*ion-inline-start:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\settings\settings.html"*/'<ion-content padding-top>\n\n  <div align="center" style="margin-top: 20%;">\n\n    <img class="profile-image" src="assets/imgs/dummy_user.jpg">\n\n    <h6 style="font-size: 2.4rem;">{{userdetails.fn}}&nbsp;{{userdetails.ln}}</h6>\n\n    <div style="font-size:15px; text-align: center;">{{userdetails.email}}</div><br />\n\n    <div style="font-size:12px; text-align: center;">{{userdetails.phn}}</div>\n\n  </div>\n\n  <ion-card>\n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-icon ios="ios-ionic" md="md-ionic" item-start></ion-icon>\n\n        <ion-label>Version</ion-label>\n\n        <span item-end>{{aVer}}</span>\n\n      </ion-item>\n\n      <ion-item (click)="call(8956628472)">\n\n        <ion-icon name="call" item-start></ion-icon>\n\n        +91 8956628472\n\n      </ion-item>\n\n      <ion-item (click)="logout()">\n\n        <ion-icon ios="ios-log-out" md="md-log-out" item-start></ion-icon>\n\n        Log Out\n\n      </ion-item>\n\n    </ion-list>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\settings\settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["App"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_app_version__["a" /* AppVersion */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_caller_api_caller__["a" /* ApiCallerProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_call_number__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ })

});
//# sourceMappingURL=5.js.map