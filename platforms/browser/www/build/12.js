webpackJsonp([12],{

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(502);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
            ],
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 502:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_caller_api_caller__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_push__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, httpUrl, toastCtrl, alertCtrl, storage, network, push, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpUrl = httpUrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.network = network;
        this.push = push;
        this.events = events;
        this.loginDetails = {};
        this.schedules = [];
        console.log("login details=> " + localStorage.getItem('details'));
        this.loginDetails = JSON.parse(localStorage.getItem('details'));
        ///////////////////////////////
        this.events.publish('user:updated', this.loginDetails);
        /////////////////////////////////
    }
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HomePage');
    };
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.connected = this.network.onConnect().subscribe(function (data) {
            _this.getSchedules();
        }, function (error) { return console.log(error); });
        this.disconnected = this.network.onDisconnect().subscribe(function (data) {
            _this.displayNetworkUpdate(data.type);
        }, function (error) { return console.log(error); });
    };
    HomePage.prototype.ionViewWillLeave = function () {
        this.connected.unsubscribe();
        this.disconnected.unsubscribe();
        debugger;
        if (this.inetrvalId) {
            console.log("interval id cleared");
            // this.inetrvalId.clear();
            clearInterval(this.inetrvalId);
        }
    };
    HomePage.prototype.displayNetworkUpdate = function (connectionState) {
        // let networkType = this.network.type;
        this.toastCtrl.create({
            // message: `You are now ${connectionState} via ${networkType}`,
            message: "You are now " + connectionState,
            duration: 3000,
            position: 'bottom'
        }).present();
    };
    HomePage.prototype.ngOnInit = function () {
        this.getSchedules();
        // this.inetrvalId = setInterval(() => {
        //   this.getSchedules();
        // }, 10000);
        // console.log("interval: ", this.inetrvalId);
    };
    HomePage.prototype.ngOnDestroy = function () {
        localStorage.removeItem("loader-Loaded");
        // localStorage.removeItem("loader-Loaded")
        // if (this.inetrvalId) {
        //   this.inetrvalId.clear();
        // }
    };
    HomePage.prototype.getSchedules = function () {
        var _this = this;
        // debugger
        if (localStorage.getItem("loader-Loaded") === null) {
            this.httpUrl.startLoading().present();
        }
        this.httpUrl.getSchedules(this.loginDetails._id)
            .subscribe(function (data) {
            if (localStorage.getItem("loader-Loaded") === null) {
                localStorage.setItem("loader-Loaded", "true");
                _this.httpUrl.stopLoading();
            }
            console.log("data=> ", data);
            if (data.length > 0) {
                _this.innerFunc(data);
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Schedules not found..',
                    duration: 1500,
                    position: 'middle'
                });
                toast.present();
            }
            _this.addPushNotify();
        }, function (err) {
            if (localStorage.getItem("loader-Loaded") === null) {
                _this.httpUrl.stopLoading();
            }
            // this.httpUrl.stopLoading();
            console.log("error got=> ", err);
        });
    };
    HomePage.prototype.innerFunc = function (sched) {
        var that = this;
        that.schedules = [];
        var i = 0, howManyTimes = sched.length;
        function f() {
            that.schedules.push({
                'scheduleschools': sched[i].scheduleschools,
                '_id': sched[i]._id
            });
            if (sched[i]._id != null && sched[i]._id != undefined) {
                that.getTripStat(sched[i]);
            }
            else {
                that.schedules[that.schedules.length - 1].tripStat = 'N/A';
            }
            console.log("finally data: " + that.schedules);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 200);
            }
        }
        f();
    };
    HomePage.prototype.getTripStat = function (data) {
        var that = this;
        var postData = {
            "_find": {
                "createdAt": {
                    "$gte": {
                        "_eval": "dayStart",
                        "value": new Date().toISOString()
                    }
                },
                "ScheduleSchool": {
                    "_eval": "Id",
                    "value": data._id
                }
            }
        };
        that.httpUrl.tripStatusCall(postData)
            .subscribe(function (respData) {
            // that.httpUrl.stopLoading();
            console.log('response data', respData);
            if (respData.length == 0) {
                that.schedules[that.schedules.length - 1].tripStat = 'Trip not started yet';
            }
            else {
                if (respData[0].pickup == "START") {
                    that.schedules[that.schedules.length - 1].tripStat = 'Pickup Ongoing';
                }
                else {
                    if (respData[0].drop == "START") {
                        that.schedules[that.schedules.length - 1].tripStat = 'Drop Ongoing';
                    }
                    else {
                        if (respData[0].pickup == "END") {
                            that.schedules[that.schedules.length - 1].tripStat = 'Pickup Completed';
                        }
                        else {
                            if (respData[0].drop == "END") {
                                that.schedules[that.schedules.length - 1].tripStat = 'Drop Completed';
                            }
                        }
                    }
                }
            }
        }, function (err) {
            // that.httpUrl.stopLoading();
        });
    };
    HomePage.prototype.timeoutAlert = function () {
        var _this = this;
        var alerttemp = this.alertCtrl.create({
            message: "Server is taking much time to respond. Please try in some time.",
            buttons: [{
                    text: 'Okay',
                    handler: function () {
                        _this.navCtrl.setRoot("TabsPage");
                    }
                }]
        });
        alerttemp.present();
    };
    HomePage.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getSchedules();
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 200);
    };
    HomePage.prototype.pushSetup = function () {
        var _this = this;
        // Create a channel (Android O and above). You'll need to provide the id, description and importance properties.
        this.push.createChannel({
            id: "bus_reached",
            description: "bus_reached description",
            sound: 'bus_reached',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('bus_reached Channel created'); });
        this.push.createChannel({
            id: "student_deboarded",
            description: "student_deboarded description",
            sound: 'student_deboarded',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('student_deboarded Channel created'); });
        this.push.createChannel({
            id: "student_onborded",
            description: "student_onborded description",
            sound: 'student_onborded',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('student_onborded Channel created'); });
        this.push.createChannel({
            id: "default",
            description: "default description",
            sound: 'default',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('default Channel created'); });
        // Delete a channel (Android O and above)
        // this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));
        // Return a list of currently configured channels
        this.push.listChannels().then(function (channels) { return console.log('List of channels', channels); });
        // to initialize push notifications
        var that = this;
        var options = {
            android: {
                senderID: '644983599736',
                icon: 'safety365',
                iconColor: 'red'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'true'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };
        var pushObject = that.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            // if (localStorage.getItem("notifValue") != null) {
            //   if (localStorage.getItem("notifValue") == 'true') {
            // this.tts.speak(notification.message)
            //   .then(() => console.log('Success'))
            //   .catch((reason: any) => console.log(reason));
            //   }
            // }
            if (notification.additionalData.foreground) {
                var toast = _this.toastCtrl.create({
                    message: notification.message,
                    duration: 2000
                });
                toast.present();
            }
            else {
                // this.nav.setRoot('AllNotificationsPage');
            }
        });
        pushObject.on('registration')
            .subscribe(function (registration) {
            // alert(registration.registrationId)
            console.log("device token => " + registration.registrationId);
            // console.log("reg type=> " + registration.registrationType);
            localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
            // that.storage.set("DEVICE_TOKEN", registration.registrationId);
        });
        pushObject.on('error').subscribe(function (error) {
            console.error('Error with Push plugin', error);
            // alert('Error with Push plugin' + error)
        });
    };
    // pushSetup() {
    //   // to initialize push notifications
    //   let that = this;
    //   const options: PushOptions = {
    //     android: {
    //       senderID: '644983599736',
    //       icon: 'safety365',
    //       iconColor: 'red'
    //     },
    //     ios: {
    //       alert: 'true',
    //       badge: true,
    //       sound: 'true'
    //     },
    //     windows: {},
    //     browser: {
    //       pushServiceURL: 'http://push.api.phonegap.com/v1/push'
    //     }
    //   };
    //   const pushObject: PushObject = that.push.init(options);
    //   pushObject.on('registration')
    //     .subscribe((registration: any) => {
    //       console.log("device token => " + registration.registrationId);
    //       localStorage.setItem("DEVICE_TOKEN", registration.registrationId)
    //       this.storage.set("DEVICE_TOKEN", registration.registrationId);
    //       this.addPushNotify();
    //     });
    //   pushObject.on('error').subscribe(error => {
    //     console.error('Error with Push plugin', error)
    //   });
    // }
    HomePage.prototype.addPushNotify = function () {
        var _this = this;
        // console.log("push notify click")
        this.storage.get("DEVICE_TOKEN").then(function (res) {
            if (res) {
                console.log("device token=> " + JSON.stringify(res));
                _this.token = res;
                var pushdata = {
                    "uid": _this.loginDetails._id,
                    "token": _this.token,
                    "os": "android",
                    "appName": "Safety365"
                };
                // console.log("pushdata=> " + JSON.stringify(pushdata));
                _this.httpUrl.pushnotifyCall(pushdata)
                    .subscribe(function (data) {
                    _this.pushnotify = data.message;
                    console.log("data of push=> " + JSON.stringify(data));
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                _this.pushSetup();
            }
        });
        // debugger;
    };
    HomePage.prototype.showTrip = function (data) {
        var _this = this;
        console.log('show Trip', data);
        var postData = {
            "_find": {
                "createdAt": {
                    "$gte": {
                        "_eval": "dayStart",
                        "value": new Date().toISOString()
                    }
                },
                "ScheduleSchool": {
                    "_eval": "Id",
                    "value": data._id
                }
            }
        };
        this.httpUrl.startLoading().present();
        this.httpUrl.tripStatusCall(postData)
            .subscribe(function (respData) {
            console.log('response data', respData);
            _this.httpUrl.stopLoading();
            if (respData.length == 0) {
                var toast = _this.toastCtrl.create({
                    message: 'This trip is yet to start.. Please check afetr some time. Thank you!',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
            }
            else {
                if (respData[0].pickup == "START" || respData[0].drop == "START") {
                    _this.navCtrl.push("LiveSingleDevice", {
                        imei_ID: data.scheduleschools.schoolVehicles.device.Device_ID,
                        param: respData[0],
                        data: data
                    });
                }
                else {
                    if (respData[0].pickup == "END" || respData[0].drop == "END") {
                        _this.navCtrl.push("HistoryMapPage", {
                            imei_ID: data.scheduleschools.schoolVehicles.device.Device_ID,
                            param: respData[0]
                        });
                    }
                }
            }
        }, function (err) {
            _this.httpUrl.stopLoading();
            console.log("error occured=> ", err);
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\home\home.html"*/'<ion-header>\n\n  <ion-navbar color="customCol">\n\n    <ion-row>\n\n      <ion-col col-2>\n\n        <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n      </ion-col>\n\n      <ion-col col-8>\n\n        <ion-title>\n\n          <div class="titleStyle">OneQlik Parents</div>\n\n        </ion-title>\n\n      </ion-col>\n\n      <ion-col col-2></ion-col>\n\n    </ion-row>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <!-- <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles"\n\n      refreshingText="Refreshing...">\n\n    </ion-refresher-content>\n\n  </ion-refresher> -->\n\n  <ion-card style="background-color: #c4cacf; letter-spacing: 1.5px;" *ngFor="let sch of schedules;"\n\n    (tap)="showTrip(sch)">\n\n\n\n    <ion-card-header class="headClass">\n\n      <ion-row>\n\n        <ion-col col-12>\n\n          <h2 style="font-size: 2.6rem">{{sch.scheduleschools.scheduleName}}</h2>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col col-12>{{sch.scheduleschools.schoolVehicles.name}}</ion-col>\n\n      </ion-row>\n\n      <ion-row *ngIf="sch.scheduleschools.trackRoute != undefined">\n\n        <ion-col col-12>\n\n          {{sch.scheduleschools.trackRoute.name}}\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-header>\n\n\n\n    <ion-card-content>\n\n      <ion-row>\n\n        <ion-col col-6 style="text-align: center; background-color: #ef4138;">\n\n\n\n          <h4 style="color: #000; font-size: 2.4rem">{{sch.scheduleschools.pickup | date:\'shortTime\'}}</h4>\n\n          <h3 style="color:#757575;">PICKUP</h3>\n\n        </ion-col>\n\n        <ion-col col-6 style="text-align: center; background-color: #faa11f;">\n\n          <h4 style="color: #000; font-size: 2.4rem">{{sch.scheduleschools.drop | date:\'shortTime\'}}</h4>\n\n          <h3 style="color:#757575;">DROP</h3>\n\n\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col col-12 style="text-align: center; background-color: #343a40; color:#fff; font-weight: bold">\n\n          {{sch.tripStat}}\n\n        </ion-col>\n\n        <!-- <ion-col col-12 style="text-align: center; background-color: #343a40; color:#fff">\n\n          <ion-icon name="car" color="customCol"></ion-icon>\n\n          &nbsp;{{sch.scheduleschools.schoolVehicles.device.Device_Name}}\n\n        </ion-col> -->\n\n      </ion-row>\n\n    </ion-card-content>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\home\home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_caller_api_caller__["a" /* ApiCallerProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=12.js.map