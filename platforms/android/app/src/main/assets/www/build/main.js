webpackJsonp([16],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiCallerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ApiCallerProvider = /** @class */ (function () {
    function ApiCallerProvider(http, loadingCtrl, events) {
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        // service = new google.maps.DistanceMatrixService();
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
        this.link = "http://139.59.50.225/";
        this.travelDetailsObject = {};
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        if (this.token == undefined) {
            if (localStorage.getItem("AuthToken") != null) {
                this.token = localStorage.getItem("AuthToken");
                this.headers.append('Authorization', this.token);
            }
        }
    }
    // getTravelDetails(ori, dest) {
    // this.service.getDistanceMatrix({
    //     origins: [new google.maps.LatLng(ori.lat, ori.lng)],
    //     destinations: [new google.maps.LatLng(dest.lat, dest.lng)],
    //     travelMode: 'DRIVING'
    //   }, this.callback);
    // }
    // callback(response, status) {
    //   // debugger
    //   let travelDetailsObject;
    //   if (status == 'OK') {
    //     var origins = response.originAddresses;
    //     var destinations = response.destinationAddresses;
    //     for (var i = 0; i < origins.length; i++) {
    //       var results = response.rows[i].elements;
    //       for (var j = 0; j < results.length; j++) {
    //         var element = results[j];
    //         var distance = element.distance.text;
    //         var duration = element.duration.text;
    //         var from = origins[i];
    //         var to = destinations[j];
    //         travelDetailsObject = {
    //           distance: distance,
    //           duration: duration
    //         }
    //       }
    //     }
    //     this.travelDetailsObject = travelDetailsObject;
    //     console.log("distance matrix: ", travelDetailsObject)
    //   }
    // }
    ApiCallerProvider.prototype.startLoading = function () {
        return this.loading = this.loadingCtrl.create({
            content: "Please wait...",
            spinner: "bubbles"
        });
    };
    ApiCallerProvider.prototype.stopLoading = function () {
        return this.loading.dismiss();
    };
    ApiCallerProvider.prototype.getdevicesApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.getUrl = function (url) {
        return this.http.get(url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.getPoiCall = function (_id) {
        // 5b0bfedc43b09153a28c0b65
        return this.http.get("https://www.oneqlik.in/trackroute/sbUsers/routePoi?user=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.loginCall = function (data) {
        return this.http.post(this.link + "users/LoginWithOtp", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.getDeviceByUserCall = function (userdetails) {
        return this.http.get("http://13.126.36.205:3000/devices/getDeviceByUser?id=" + userdetails._id + "&email=" + userdetails.email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.getRecentNotifCall = function (_id) {
        return this.http.get(this.link + 'notifs/getRecent?user=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.pushnotifyCall = function (pushdata) {
        return this.http.post(this.link + "users/PushNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.getSchedules = function (_id) {
        this.token = undefined;
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        if (localStorage.getItem("AuthToken") != null) {
            this.token = localStorage.getItem("AuthToken");
            console.log("Headers token => ", this.token);
            this.headers.append('Authorization', this.token);
        }
        console.log("headers=> " + JSON.stringify(this.headers));
        return this.http.get(this.link + "student/parentScheduled?id=" + _id, { headers: this.headers })
            .timeout(15000)
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.tripStatusCall = function (data) {
        // debugger
        this.token = undefined;
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        if (localStorage.getItem("AuthToken") != null) {
            this.token = localStorage.getItem("AuthToken");
            console.log("Headers token => ", this.token);
            this.headers.append('Authorization', this.token);
        }
        // this.headnew = new Headers({ 'Content-Type': 'application/json; charset=utf-8', 'Authorization': this.token });
        return this.http.post(this.link + 'schoolTrip/find', data, { headers: this.headers })
            .timeout(15000)
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.stoppage_detail = function (_id, from, to, device_id) {
        return this.http.get(this.link + 'stoppage/stoppage_detail?uId=' + _id + '&from_date=' + from + '&to_date=' + to + '&device=' + device_id, { headers: this.headers })
            .timeout(15000)
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.gpsCall = function (device_id, from, to) {
        return this.http.get(this.link + 'gps/v2?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .timeout(15000)
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.getDistanceSpeedCall = function (device_id, from, to) {
        return this.http.get(this.link + 'gps/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .timeout(15000)
            .map(function (res) { return res.json(); });
    };
    // Rahul update service/////
    ApiCallerProvider.prototype.getStudent = function (data) {
        return this.http.post("http://139.59.50.225/student/find", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.studentlistreportCall = function (data) {
        return this.http.post("http://139.59.50.225/attendance/studentList", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.ReportCall = function (data) {
        // return this.http.post("http://www.thingslab.in/attendance/studentDayWiseReport", data, { headers: this.headers })
        return this.http.post("http://139.59.50.225/attendance/studentDayWiseReport1", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.studenEdit = function (data) {
        return this.http.post("http://139.59.50.225/student/edit", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.getprofileApi = function (_id) {
        return this.http.get("http://139.59.50.225/users/parentData?id=" + _id, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.getProfilestudentApi = function (_id) {
        return this.http.get("http://139.59.50.225/users/parentData?id=" + _id, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.shareLivetrackCall = function (data) {
        return this.http.post("http://139.59.50.225/share", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.getTrackrouteCall = function (id, user) {
        // 5b0bfedc43b09153a28c0b65
        return this.http.get("http://139.59.50.225/trackRoute/routepath/getRoutePathWithPoi?id=" + id + '&user=' + user, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider.prototype.pullnotifyCall = function (pushdata) {
        return this.http.post("http://139.59.50.225/users/PullNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiCallerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["LoadingController"], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["Events"]])
    ], ApiCallerProvider);
    return ApiCallerProvider;
}());

//# sourceMappingURL=api-caller.js.map

/***/ }),

/***/ 115:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 115;

/***/ }),

/***/ 157:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/assignment/assignment.module": [
		300,
		15
	],
	"../pages/assignment/details/details.module": [
		301,
		14
	],
	"../pages/fee/fee.module": [
		302,
		13
	],
	"../pages/history-map/history-map.module": [
		314,
		2
	],
	"../pages/home/home.module": [
		313,
		12
	],
	"../pages/live-single-device/live-single-device.module": [
		315,
		0
	],
	"../pages/login/login.module": [
		303,
		11
	],
	"../pages/notification/notification.module": [
		304,
		3
	],
	"../pages/profile/profile.module": [
		305,
		10
	],
	"../pages/reports/detailed-report/detailed-report.module": [
		306,
		1
	],
	"../pages/reports/reports.module": [
		307,
		9
	],
	"../pages/results/result-details/result-details.module": [
		308,
		8
	],
	"../pages/results/results.module": [
		309,
		7
	],
	"../pages/settings/settings.module": [
		310,
		5
	],
	"../pages/tabs/tabs.module": [
		311,
		4
	],
	"../pages/trips/trips.module": [
		312,
		6
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 157;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConnectivityServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(104);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { HttpClient } from '@angular/common/http';



var ConnectivityServiceProvider = /** @class */ (function () {
    function ConnectivityServiceProvider(platform, alertCtrl, network) {
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.network = network;
        console.log('Hello ConnectivityServiceProvider Provider');
        this.onDevice = this.platform.is('cordova');
    }
    ConnectivityServiceProvider.prototype.isOnline = function () {
        if (this.onDevice && this.network.type) {
            return this.network.type !== 'none';
        }
        else {
            return navigator.onLine;
        }
    };
    ConnectivityServiceProvider.prototype.isOffline = function () {
        if (this.onDevice && this.network.type) {
            return this.network.type === 'none';
        }
        else {
            return !navigator.onLine;
        }
    };
    ConnectivityServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */]])
    ], ConnectivityServiceProvider);
    return ConnectivityServiceProvider;
}());

//# sourceMappingURL=connectivity-service.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(233);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_google_maps__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__node_modules_ion_bottom_drawer__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_push__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_geolocation__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_app_version__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_social_sharing__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_text_to_speech__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_geocoder_geocoder__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_api_caller_api_caller__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_connectivity_service_connectivity_service__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_diagnostic__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_uid__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_storage__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_in_app_browser__ = __webpack_require__(208);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















// import { Pro } from '@ionic/pro';
// import { Injectable, Injector } from '@angular/core';

// import { TabsPage } from '../pages/tabs/tabs';





// Pro.init('206faa80', {
//   appVersion: '2.1'
// })
// @Injectable()
// export class MyErrorHandler implements ErrorHandler {
//   ionicErrorHandler: IonicErrorHandler;
//   constructor(injector: Injector) {
//     try {
//       this.ionicErrorHandler = injector.get(IonicErrorHandler);
//     } catch (e) {
//       // Unable to get the IonicErrorHandler provider, ensure
//       // IonicErrorHandler has been added to the providers list below
//     }
//   }
//   handleError(err: any): void {
//     Pro.monitoring.handleNewError(err);
//     // Remove this if you want to disable Ionic's auto exception handling
//     // in development mode.
//     this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
//   }
// }
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/assignment/assignment.module#AssignmentPageModule', name: 'AssignmentPage', segment: 'assignment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/assignment/details/details.module#DetailsPageModule', name: 'DetailsPage', segment: 'details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fee/fee.module#FeePageModule', name: 'FeePage', segment: 'fee', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notification/notification.module#NotificationPageModule', name: 'NotificationPage', segment: 'notification', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reports/detailed-report/detailed-report.module#DetailedReportPageModule', name: 'DetailedReportPage', segment: 'detailed-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reports/reports.module#ReportsPageModule', name: 'ReportsPage', segment: 'reports', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/results/result-details/result-details.module#ResultDetailsPageModule', name: 'ResultDetailsPage', segment: 'result-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/results/results.module#ResultsPageModule', name: 'ResultsPage', segment: 'results', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trips/trips.module#TripsPageModule', name: 'TripsPage', segment: 'trips', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/history-map/history-map.module#HistoryMapPageModule', name: 'HistoryMapPage', segment: 'history-map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live-single-device/live-single-device.module#LiveSingleDeviceModule', name: 'LiveSingleDevice', segment: 'live-single-device', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_22__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_11__node_modules_ion_bottom_drawer__["a" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"],
                // [{ provide: ErrorHandler, useClass: MyErrorHandler }],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"] },
                __WEBPACK_IMPORTED_MODULE_18__providers_api_caller_api_caller__["a" /* ApiCallerProvider */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_19__providers_connectivity_service_connectivity_service__["a" /* ConnectivityServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_google_maps__["b" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_google_maps__["i" /* Spherical */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_app_version__["a" /* AppVersion */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_text_to_speech__["a" /* TextToSpeech */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_diagnostic__["a" /* Diagnostic */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_uid__["a" /* Uid */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_17__providers_geocoder_geocoder__["a" /* GeocoderProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_push__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_text_to_speech__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_api_caller_api_caller__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_uid__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__node_modules_ionic_native_diagnostic__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, events, push, alertCtrl, app, apiCall, toastCtrl, tts, uid, diagnostic, storage) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.events = events;
        this.push = push;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.tts = tts;
        this.uid = uid;
        this.diagnostic = diagnostic;
        this.storage = storage;
        this.pages = [
            { title: 'Home', pageName: 'TabsPage', tabComponent: 'Tab1Page', index: 0, icon: 'home' },
            // { title: 'Tab 2', pageName: 'TabsPage', tabComponent: 'Tab2Page', index: 1, icon: 'contacts' },
            { title: 'Fee', pageName: 'FeePage', icon: 'cash' },
            { title: 'Assignment', pageName: 'AssignmentPage', icon: 'create' },
            { title: 'Results', pageName: 'ResultsPage', icon: 'checkmark' },
        ];
        this.islogin = {};
        platform.ready().then(function () {
            _this.getPermission();
            statusBar.styleDefault();
            statusBar.hide();
            _this.splashScreen.hide();
            platform.registerBackButtonAction(function () {
                var nav = _this.app.getActiveNavs()[0];
                var activeView = nav.getActive();
                if (nav.canGoBack()) {
                    nav.pop(); // IF IT'S NOT THE ROOT, POP A PAGE.
                }
                else {
                    platform.exitApp(); // IF IT'S THE ROOT, EXIT THE APP.
                }
                // }
            });
        });
        this.pushNotify();
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.initializeApp();
        if (localStorage.getItem("LOGGED_IN")) {
            this.rootPage = 'TabsPage';
        }
        else {
            this.rootPage = 'LoginPage';
        }
        this.events.subscribe('user:updated', function (udata) {
            _this.islogin = udata;
            _this.isDealer = udata.isDealer;
            console.log("islogin=> " + JSON.stringify(_this.islogin));
        });
    }
    MyApp.prototype.getPermission = function () {
        var _this = this;
        this.diagnostic.getPermissionAuthorizationStatus(this.diagnostic.permission.READ_PHONE_STATE).then(function (status) {
            if (status != _this.diagnostic.permissionStatus.GRANTED) {
                _this.diagnostic.requestRuntimePermission(_this.diagnostic.permission.READ_PHONE_STATE).then(function (data) {
                    var imei = _this.uid.IMEI;
                });
            }
            else {
                console.log("We have the permission");
                localStorage.setItem("IMEI", _this.uid.IMEI);
                localStorage.setItem("UUID", _this.uid.UUID);
            }
        }, function (statusError) {
            console.log("statusError=> " + statusError);
        });
    };
    MyApp.prototype.pushNotify = function () {
        var that = this;
        that.push.hasPermission() // to check if we have permission
            .then(function (res) {
            if (res.isEnabled) {
                console.log('We have permission to send push notifications');
                that.pushSetup();
            }
            else {
                that.pushSetup();
                console.log('We do not have permission to send push notifications');
            }
        });
    };
    MyApp.prototype.pushSetup = function () {
        var _this = this;
        // Create a channel (Android O and above). You'll need to provide the id, description and importance properties.
        this.push.createChannel({
            id: "bus_reached",
            description: "bus_reached description",
            sound: 'bus_reached',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('bus_reached Channel created'); });
        this.push.createChannel({
            id: "student_deboarded",
            description: "student_deboarded description",
            sound: 'student_deboarded',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('student_deboarded Channel created'); });
        this.push.createChannel({
            id: "student_onborded",
            description: "student_onborded description",
            sound: 'student_onborded',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('student_onborded Channel created'); });
        this.push.createChannel({
            id: "default",
            description: "default description",
            sound: 'default',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('default Channel created'); });
        // Delete a channel (Android O and above)
        // this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));
        // Return a list of currently configured channels
        this.push.listChannels().then(function (channels) { return console.log('List of channels', channels); });
        // to initialize push notifications
        var that = this;
        var options = {
            android: {
                senderID: '644983599736',
                icon: 'safety365',
                iconColor: 'red'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'true'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };
        var pushObject = that.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            // if (localStorage.getItem("notifValue") != null) {
            //   if (localStorage.getItem("notifValue") == 'true') {
            _this.tts.speak(notification.message)
                .then(function () { return console.log('Success'); })
                .catch(function (reason) { return console.log(reason); });
            //   }
            // }
            if (notification.additionalData.foreground) {
                var toast = _this.toastCtrl.create({
                    message: notification.message,
                    duration: 2000
                });
                toast.present();
            }
            else {
                // this.nav.setRoot('AllNotificationsPage');
            }
        });
        pushObject.on('registration')
            .subscribe(function (registration) {
            // alert(registration.registrationId)
            console.log("device token => " + registration.registrationId);
            // console.log("reg type=> " + registration.registrationType);
            localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
            // that.storage.set("DEVICE_TOKEN", registration.registrationId);
        });
        pushObject.on('error').subscribe(function (error) {
            console.error('Error with Push plugin', error);
            // alert('Error with Push plugin' + error)
        });
    };
    MyApp.prototype.initializeApp = function () {
        var that = this;
        that.platform.ready().then(function () {
            that.pushNotify();
        });
    };
    MyApp.prototype.openPage = function (page) {
        var params = {};
        // The index is equal to the order of our tabs inside tabs.ts
        if (page.index) {
            params = { tabIndex: page.index };
        }
        // The active child nav is our Tabs Navigation
        if (this.nav.getActiveChildNav() && page.index != undefined) {
            this.nav.getActiveChildNav().select(page.index);
        }
        else {
            // Tabs are not active, so reset the root page 
            // In this case: moving to or from SpecialPage
            this.nav.setRoot(page.pageName, params);
        }
    };
    MyApp.prototype.isActive = function (page) {
        // Again the Tabs Navigation
        var childNav = this.nav.getActiveChildNav();
        if (childNav) {
            if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
                return 'primary';
            }
            return;
        }
        // Fallback needed when there is no active childnav (tabs not active)
        if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
            return 'primary';
        }
        return;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-main-page',template:/*ion-inline-start:"D:\New\oneqlik-school-bus-tracking-parent-app\src\app\app.html"*/'<ion-menu [content]="content">\n\n    <ion-header>\n\n        <!-- <ion-toolbar>\n\n            <ion-title>Menu</ion-title>\n\n        </ion-toolbar> -->\n\n    </ion-header>\n\n\n\n    <ion-content id="outerNew">\n\n        <div class="headProf" #headerTag ion-fixed>\n\n            <img *ngIf="!fileUrl" src="assets/imgs/dummy_user.jpg" />\n\n            <img *ngIf="fileUrl" src="{{fileUrl}}" />\n\n            <div>\n\n                <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n\n                <p style="font-size: 12px">\n\n                    <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}\n\n                </p>\n\n                <p style="font-size: 12px">\n\n                    <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}\n\n                </p>\n\n            </div>\n\n        </div>\n\n        <div #scrollableTag style="margin-top: 65%;">\n\n            <ion-list>\n\n                <!-- <ion-item> -->\n\n                    <button ion-item menuClose *ngFor="let p of pages" (click)="openPage(p)" style="background-color: transparent; color: white;">\n\n                        <ion-icon item-start [name]="p.icon" [color]="isActive(p)"></ion-icon>\n\n                        {{ p.title }}\n\n                    </button>\n\n                <!-- </ion-item> -->\n\n            </ion-list>\n\n        </div>\n\n    </ion-content>\n\n</ion-menu>\n\n\n\n<!-- main navigation -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"D:\New\oneqlik-school-bus-tracking-parent-app\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["App"],
            __WEBPACK_IMPORTED_MODULE_6__providers_api_caller_api_caller__["a" /* ApiCallerProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_text_to_speech__["a" /* TextToSpeech */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_uid__["a" /* Uid */],
            __WEBPACK_IMPORTED_MODULE_8__node_modules_ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeocoderProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_geocoder__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GeocoderProvider = /** @class */ (function () {
    function GeocoderProvider(_GEOCODE) {
        this._GEOCODE = _GEOCODE;
        console.log('Hello GeocoderProvider Provider');
    }
    GeocoderProvider.prototype.reverseGeocode = function (lat, lng) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._GEOCODE.reverseGeocode(lat, lng)
                .then(function (result) {
                var a = result[0].thoroughfare ? result[0].thoroughfare : null;
                var b = result[0].subThoroughfare ? result[0].subThoroughfare : null;
                var c = result[0].subLocality ? result[0].subLocality : null;
                var d = result[0].subAdministrativeArea ? result[0].subAdministrativeArea : null;
                var e = result[0].postalCode ? result[0].postalCode : null;
                var f = result[0].locality ? result[0].locality : null;
                var g = result[0].countryName ? result[0].countryName : null;
                var h = result[0].administrativeArea ? result[0].administrativeArea : null;
                var str = '';
                if (a != null && a != 'Unnamed Road')
                    str = a + ', ';
                if (b != null && b != 'Unnamed Road')
                    str = str + b + ', ';
                if (c != null && c != 'Unnamed Road')
                    str = str + c + ', ';
                if (d != null && d != 'Unnamed Road')
                    str = str + d + ', ';
                if (e != null && e != 'Unnamed Road')
                    str = str + e + ', ';
                if (f != null && f != 'Unnamed Road')
                    str = str + f + ', ';
                if (g != null && g != 'Unnamed Road')
                    str = str + g + ', ';
                if (h != null && h != 'Unnamed Road')
                    str = str + h + ', ';
                resolve(str);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    GeocoderProvider.prototype.geocoderResult = function (lat, lng) {
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["a" /* Geocoder */].geocode({
                "position": {
                    lat: lat,
                    lng: lng
                }
            }).then(function (results) {
                var addr;
                if (results.length == 0) {
                    addr = 'N/A';
                    resolve(addr);
                }
                else {
                    addr = results[0].extra.lines[0];
                    resolve(addr);
                }
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    GeocoderProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_native_geocoder__["a" /* NativeGeocoder */]])
    ], GeocoderProvider);
    return GeocoderProvider;
}());

//# sourceMappingURL=geocoder.js.map

/***/ })

},[211]);
//# sourceMappingURL=main.js.map