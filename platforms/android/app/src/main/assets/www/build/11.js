webpackJsonp([11],{

/***/ 303:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(490);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 490:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_caller_api_caller__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_connectivity_service_connectivity_service__ = __webpack_require__(207);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, apiCall, toastCtrl, alertCtrl, connectivityService, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.connectivityService = connectivityService;
        this.events = events;
        this.internetStatus = false;
        this.isParent = false;
        this.showPassword = false;
        this.checkInternetConnection();
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.toggleShowPassword = function () {
        this.showPassword = !this.showPassword;
    };
    LoginPage.prototype.checkInternetConnection = function () {
        this.addConnectivityListeners();
        if (this.connectivityService.isOffline()) {
            this.internetStatus = true;
        }
    };
    LoginPage.prototype.addConnectivityListeners = function () {
        var _this = this;
        var onOnline = function () {
            _this.internetStatus = false;
        };
        var onOffline = function () {
            _this.internetStatus = true;
        };
        document.addEventListener('online', onOnline, false);
        document.addEventListener('offline', onOffline, false);
    };
    LoginPage.prototype.userLogin = function (loginForm) {
        var _this = this;
        var userData = {};
        var user = isNaN(loginForm.value.username);
        if (user == false) {
            userData = {
                "psd": loginForm.value.password,
                "ph_num": loginForm.value.username
            };
        }
        else {
            userData = {
                "psd": loginForm.value.password,
                "emailid": loginForm.value.username
            };
        }
        if (this.internetStatus == true) {
            var alert_1 = this.alertCtrl.create({
                title: 'Network Alert',
                message: 'Please check your network connection and try again...',
                buttons: ['OK']
            });
            alert_1.present();
        }
        this.apiCall.startLoading().present();
        this.apiCall.loginCall(userData)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("response data token => ", data.token);
            _this.events.publish("auth:token", data.token);
            localStorage.setItem("AuthToken", data.token);
            var logindata = JSON.stringify(data);
            var logindetails = JSON.parse(logindata);
            var userdetials = window.atob(logindetails.token.split('.')[1]);
            var details = JSON.parse(userdetials);
            localStorage.setItem('details', JSON.stringify(details)); // storing user details in localstorage
            var toast = _this.toastCtrl.create({
                message: "Welcome! You're logged In successfully.",
                duration: 2000,
                position: 'top'
            });
            toast.onDidDismiss(function () {
                // console.log('Dismissed toast');
                localStorage.setItem("LOGGED_IN", "LOGGED_IN");
                _this.navCtrl.setRoot("TabsPage");
            });
            toast.present();
        }, function (error) {
            console.log(error);
            _this.apiCall.stopLoading();
            var body = error._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: "Login Failed!",
                message: msg.message,
                buttons: ['Try Again']
            });
            alert.present();
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\login\login.html"*/'<ion-content class=outer>\n\n  <div class="warning">\n\n    <div id=inner>\n\n      <div class="logo">\n\n        <img src="assets/imgs/icon.png" width="180" height="180">\n\n        <!-- <ion-img src="assets/imgs/school-buss-tracker-icon.png" width="280" height="100"></ion-img> -->\n\n      </div>\n\n      <form #loginForm="ngForm">\n\n        <ion-list>\n\n          <!-- <div class="temp"> -->\n\n            <ion-item class="itemDiv">\n\n              <ion-label floating>Email/Mobile No.</ion-label>\n\n              <ion-input type="text" [(ngModel)]="username" name="username" required></ion-input>\n\n            </ion-item>\n\n          <!-- </div> -->\n\n          <!-- <div class="temp"> -->\n\n            <ion-item class="itemDiv">\n\n              <ion-label floating>Password</ion-label>\n\n              <ion-input [(ngModel)]="password" *ngIf="showPassword" type="text" name="password" required></ion-input>\n\n              <ion-input type="password" *ngIf="!showPassword" [(ngModel)]="password" name="password" required></ion-input>\n\n\n\n              <button ion-button clear item-end (click)="toggleShowPassword()">\n\n                <ion-icon style="font-size: 1.6em;" *ngIf="showPassword" name="eye" color="light"></ion-icon>\n\n                <ion-icon style="font-size: 1.6em;" *ngIf="!showPassword" name="eye-off" color="light"></ion-icon>\n\n              </button>\n\n            </ion-item>\n\n          <!-- </div> -->\n\n        </ion-list>\n\n        <button ion-button block color="customCol" [disabled]="!loginForm.form.valid" (click)="userLogin(loginForm)">SIGN IN</button>\n\n      </form>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\New\oneqlik-school-bus-tracking-parent-app\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_caller_api_caller__["a" /* ApiCallerProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_connectivity_service_connectivity_service__["a" /* ConnectivityServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=11.js.map