import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {
  parameters: any;
  selectedDate: any = new Date().toISOString();
  items: any = [
    {
      title: "Maths",

    },
    {
      title: "Science",
      
    },
    {
      title: "English",
      
    },
    {
      title: "Geometry",
      
    }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log("nav params: ", this.navParams.get('param'))
    if(this.navParams.get("param") !== null) {
      this.parameters = this.navParams.get("param");
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPage');
  }

  getData() {

  }

  itemSelected(item) {

  }

}
