import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-assignment',
  templateUrl: 'assignment.html',
})
export class AssignmentPage {
  selectedMonth: any;
  listItems: any = [];
  index: number = 0;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AssignmentPage');
  }

  getData() {
    this.listItems.push(
      {
        id: this.index + 1,
        title: "Homework",
        status: "pending",
        timestamp: new Date().toISOString()
      },
      {
        id: this.index + 1,
        title: "Review",
        status: "pending",
        timestamp: new Date().toISOString()
      },
      {
        id: this.index + 1,
        title: "Homework",
        status: "pending",
        timestamp: new Date().toISOString()
      },
    )
  }

  details(item) {
    this.navCtrl.push('DetailsPage', {
      param: item
    });
  }

}
