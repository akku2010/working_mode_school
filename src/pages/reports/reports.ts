import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-reports',
  templateUrl: 'reports.html',
})
export class ReportsPage implements OnInit {

  loginDetails: any;
  token: string;
  deviceUUID: string;

  isstudent: string;
  SelectVehicle: string = 'Select Student';
  portstemp: any;
  selectedStudent_id: any;
  studentReport: any;
  datareport: any[];
  mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  data: { "studentId": { "_eval": string; "value": any; }; "start": { "_eval": string; "value": string; }; "end": { "_eval": string; "value": string; }; };
  selectedStudent: any;
  liststudent: any[];
  student_scheduleId: any;
  stuReport: any;
  datetimeStart: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public apiCall: ApiCallerProvider
  ) {

    console.log("login details=> " + localStorage.getItem('details'));
    this.loginDetails = JSON.parse(localStorage.getItem('details'));
    console.log("login _id=> " + this.loginDetails._id);

    this.token = localStorage.getItem("DEVICE_TOKEN");
    this.deviceUUID = localStorage.getItem("UUID");
    console.log("UUID=> ", this.deviceUUID)



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportsPage');
  }

  showDetails(childdata) {
    var combinedData = {
      "statusReport": this.stuReport,
      "studentId": this.selectedStudent_id
    }

    this.navCtrl.push("DetailedReportPage",
      {
        data: combinedData,
        param: childdata,
        childName: this.selectedStudent
      });

  }
  ngOnInit() {
    this.getStudent();

  }

  getStudent() {
    var data =
    {
      "_find": { "parent": { "_eval": "Id", "value": this.loginDetails._id } }
    }

    this.apiCall.startLoading().present();
    this.apiCall.getStudent(data)
      .subscribe(data => {
        console.log("student data", data);
        this.portstemp = data;
        this.liststudent = [];
        for (var i = 0; i < data.length; i++) {
          this.liststudent.push({ 'name': data[i].name.fname, 'id': data[i]._id, 'scheduleId': data[i].scheduledTrips })

        }
        console.log("student data name", this.liststudent);
        this.apiCall.stopLoading();

        localStorage.setItem('student', data);
        this.isstudent = localStorage.getItem('student');
      },
        error => {
          this.apiCall.stopLoading();
          console.log(error);
        });
  }


  onChangedSelect(selectedStudent) {
    console.log("selectedStudent=> ", selectedStudent)
    this.selectedStudent_id = selectedStudent.id;
    this.student_scheduleId = selectedStudent.scheduleId;
    this.selectedStudent = selectedStudent;

    console.log("selectedStudent id=> ", this.selectedStudent_id)
    var data =
    {
      "student": { "_eval": "Id", "value": this.selectedStudent_id },
      "scheduleSchool": { "_eval": "Id", "value": this.student_scheduleId }
    }


    this.apiCall.startLoading().present();
    this.apiCall.studentlistreportCall(data)
      .subscribe(data => {


        this.apiCall.stopLoading();
        console.log("student report data", data);



        let arr = data.total.filter(function (d) { return d._id; }).map(function (d) { return d._id; })
        let allRes = [];

        for (let x in arr) {
          let dd: any = {};
          dd.year = arr[x].year;
          dd.month = arr[x].month;
          var temp = parseInt(arr[x].month);



          dd.totalTrip = data.total.filter(function (d) { return d._id.year == arr[x].year && d._id.month == arr[x].month; })[0].total;

          let totalPresent = data.student.filter(function (d) { return d._id.year == arr[x].year && d._id.month == arr[x].month; });


          if (totalPresent == undefined || totalPresent.length == 0) {
            dd.totalPresent = 0;
          } else {
            dd.totalPresent = totalPresent[0].total;
          }

          // dd.totalPresent = data.student.filter(function (d) { return d._id.year == arr[x].year && d._id.month == arr[x].month; })[0].total;

          // if(dd.totalPresent== undefined){
          //   dd.totalPresent='0';
          // }else{
          //   dd.totalPresent = data.student.filter(function (d) { return d._id.year == arr[x].year && d._id.month == arr[x].month; })[0].total;

          // }

          dd.monthString = this.mL[temp - 1];

          dd.absent = (dd.totalTrip - dd.totalPresent);
          allRes.push(dd);
        }
        console.log(allRes);
        this.stuReport = allRes;

        // console.log(this.studentReport[0].statusType);
        // for (var i = 0; i < this.studentReport.length; i++) {

        //   console.log("student data name",this.studentReport[i].data.statusType);
        //   this.datareport = [];
        //   this.datareport.push({datastatusType:this.studentReport[i].data.statusType});

        // }
        // console.log("student report data", this.datareport );

        localStorage.setItem('student', this.stuReport);
        this.isstudent = localStorage.getItem('student');
      },
        error => {
          this.apiCall.stopLoading();
          console.log(error);
        });

  }

  //   ShowReportData() {
  //     var data =
  //     {
  //       "studentId": { "_eval": "Id", "value": "5a27deca4468572f678e2d91" },
  //       "start": { "_eval": "dayStart", "value": "2017-12-08T15:54:36.218+0530" },
  //       "end": { "_eval": "dayEnd", "value": "2019-12-10T11:29:28.024+0530" }
  //     }

  //     this.apiCall.startLoading().present();
  //     this.apiCall.ReportCall(data)
  //     .subscribe(data => {
  //       console.log("student report data",data);


  //       this.apiCall.stopLoading();

  //     localStorage.setItem('student',data);
  //     this.isstudent = localStorage.getItem('student');
  //  },
  //     error => {
  //       this.apiCall.stopLoading();
  //       console.log(error);
  //     });

  //   }




}
