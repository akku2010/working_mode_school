import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'page-results',
  templateUrl: 'results.html',
})
export class ResultsPage implements OnInit {
  selectedStudent: any;
  liststudent: any = [];
  loginDetails: any;
  isstudent: string;
  index: number = 0;
  resultList: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiCallerProvider,
    private iab: InAppBrowser
  ) {
    this.loginDetails = JSON.parse(localStorage.getItem('details'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultsPage');
  }

  ngOnInit() {
    this.getStudent();
  }


  getStudent() {
    var data =
    {
      "_find": { "parent": { "_eval": "Id", "value": this.loginDetails._id } }
    }

    this.apiCall.startLoading().present();
    this.apiCall.getStudent(data)
      .subscribe(data => {
        console.log("student data", data);
        // this.portstemp = data;
        this.liststudent = [];
        for (var i = 0; i < data.length; i++) {
          this.liststudent.push({ 'name': data[i].name.fname, 'id': data[i]._id, 'scheduleId': data[i].scheduledTrips })

        }
        console.log("student data name", this.liststudent);
        this.apiCall.stopLoading();

        localStorage.setItem('student', data);
        this.isstudent = localStorage.getItem('student');
      },
        error => {
          this.apiCall.stopLoading();
          console.log(error);
        });
  }

  onChangedSelect(selectedStudent) {
    console.log("selected student: ", selectedStudent);
    var url = this.apiCall.link + "users/getResult?student=" + selectedStudent.id;
    this.apiCall.startLoading().present();
    this.apiCall.getUrl(url)
      .subscribe((data) => {
        this.apiCall.stopLoading();
        console.log("response data: ", data);
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        })
    this.resultList = [];

    this.resultList.push(
      {
        id: this.index + 1,
        title: "Half Yearly",
      },
      {
        id: this.index + 1,
        title: "Yearly",
      }, {
      id: this.index + 1,
      title: "Monthly",
    }, {
      id: this.index + 1,
      title: "Quarterly",
    }, {
      id: this.index + 1,
      title: "Half Quarterly",
    },
    )
  }

  gotoNext() {
    const browser = this.iab.create('http://www.africau.edu/images/default/sample.pdf', '_system');
    browser.on('loadstop').subscribe(event => {
      browser.insertCSS({ code: "body{color: red;" });
    });
  }

}
