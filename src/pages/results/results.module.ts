import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultsPage } from './results';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    ResultsPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultsPage),
    SelectSearchableModule,
  ],
})
export class ResultsPageModule {}
