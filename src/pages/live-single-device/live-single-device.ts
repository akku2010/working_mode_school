import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ModalController, IonicPage, Platform, AlertController, ViewController, ToastController } from 'ionic-angular';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import * as io from 'socket.io-client';
import * as _ from "lodash";
import * as moment from 'moment';
//import { TranslateService } from '@ngx-translate/core';
//import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { Subscription } from 'rxjs/Subscription';
import { Polyline, LatLngBounds, MarkerCluster, ILatLng, Polygon, GoogleMapsAnimation, CircleOptions, Circle, GoogleMaps, Marker, LatLng, Spherical, GoogleMapsEvent, GoogleMapsMapTypeId } from '@ionic-native/google-maps';
import { CallNumber } from '@ionic-native/call-number';
declare var google;

@IonicPage()
@Component({
    selector: 'page-live',
    templateUrl: 'live-single-device.html',
})
export class LiveSingleDevice implements OnInit, OnDestroy {
//     service = new google.maps.DistanceMatrixService();
//     ongoingGoToPoint: any = {};
//     ongoingMoveMarker: any = {};

//     data: any = {};
//     _io: any;
//     socketSwitch: any = {};
//     socketChnl: any = [];

//     allData: any = {}; 
//     userdetails: any;
//     gpsTracking: any;

//     todays_odo: any;
//     vehicle_speed: any;

//     isEnabled: boolean = false;
//     liveDataShare: any;
//     showShareBtn: boolean = false;
//     resToken: any;
//     mapData: any[];
//     imei_id: any;
//     routeId: any;
//     routedata: any;
//     routeData: any;
//     locations: any = [];
//     profilesStudent: any;
//     markObj: any;
//     remMarker: Marker;
//     liveVehicleName: any;
//     rememberMark: any;
//     _commonVar: any = {};
//     origin: any = {};
//     destination: any = {};
//     travelDetailsObject: any = {};
//     last_ping_on: any;
//     menuActive: boolean;

//     constructor(
//         public navCtrl: NavController,
//         public navParams: NavParams,
//         public apiCall: ApiCallerProvider,
//         public actionSheetCtrl: ActionSheetController,
//         public elementRef: ElementRef,
//         public modalCtrl: ModalController,
//         public plt: Platform,
//         private callNumber: CallNumber
//     ) {
//         console.log("last ping on", this.last_ping_on);
//         this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
//         console.log("user details=> " + JSON.stringify(this.userdetails));

//         if (navParams.get("param") != null) {
//             console.log("device details=> ", navParams.get("param"));

//             this.imei_id = navParams.get("imei_ID");
//             console.log("imei => ", this.imei_id)
//         }
//         this.menuActive = false;
//     }

//     car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
//     bike = "M419 1754 l3 -44 -37 0 c-21 0 -34 4 -30 10 3 6 -1 10 -9 10 -19 0 -66 -21 -66 -30 0 -18 44 -44 71 -42 16 1 29 0 29 -2 0 -12 -24 -56 -30 -56 -4 0 -7 -6 -7 -12 0 -7 -8 -29 -16 -48 l-15 -35 -7 30 c-4 17 -8 36 -11 43 -6 19 -40 14 -69 -11 l-27 -23 16 -134 c12 -99 23 -149 41 -186 28 -57 25 -84 -10 -84 -34 0 -51 -25 -31 -45 9 -9 16 -33 16 -53 0 -30 -9 -46 -44 -84 -24 -27 -42 -48 -40 -48 2 0 -10 -21 -26 -47 -17 -27 -30 -61 -30 -79 0 -35 35 -189 52 -232 6 -15 10 -41 9 -59 -3 -34 19 -53 58 -53 12 0 21 -6 21 -14 0 -12 -7 -13 -32 -5 -44 12 -70 11 -66 -2 2 -6 21 -12 41 -14 21 -1 43 -7 49 -13 17 -17 23 -119 8 -137 -10 -12 -9 -19 3 -38 l15 -23 -46 23 c-29 15 -63 23 -94 23 -56 0 -61 -14 -17 -48 24 -20 42 -24 97 -24 63 -1 69 -3 109 -39 22 -21 38 -41 35 -44 -4 -4 -2 -5 3 -4 6 1 30 -3 55 -10 54 -16 142 -9 176 14 13 8 24 13 24 10 0 -3 16 13 36 34 35 39 36 40 103 39 59 -1 73 2 99 23 45 35 41 49 -17 49 -32 0 -64 -8 -97 -25 -27 -14 -42 -20 -33 -13 10 8 14 23 11 38 -10 54 -7 139 6 152 7 7 30 13 52 13 27 0 40 4 40 13 0 11 -9 13 -36 8 -19 -4 -42 -10 -50 -13 -10 -3 -12 0 -8 10 3 8 18 17 33 20 23 5 30 13 35 41 4 18 13 47 20 62 8 16 26 77 41 136 l26 106 -45 90 c-24 49 -56 102 -70 119 -14 17 -23 33 -19 37 3 3 1 6 -5 6 -15 0 -16 57 -1 62 22 7 -3 38 -31 38 -19 0 -31 6 -34 18 -7 20 38 114 49 104 5 -4 5 -2 2 4 -4 7 0 67 9 135 8 68 13 134 10 145 -8 29 -60 51 -81 34 -8 -7 -15 -22 -15 -35 0 -43 -13 -33 -46 36 -19 39 -34 73 -34 76 0 3 13 3 30 1 22 -3 35 2 50 17 27 29 17 49 -26 53 -22 1 -33 -2 -29 -8 4 -6 -8 -10 -29 -10 -34 0 -35 1 -38 43 -3 42 -3 42 -43 43 l-40 1 4 -43z m-140 -187 c13 -16 5 -27 -21 -27 -14 0 -18 -5 -14 -17 8 -29 26 -196 24 -220 -5 -48 -19 -2 -32 106 -16 127 -15 133 3 155 16 20 25 20 40 3z m402 -3 c7 -9 14 -22 15 -30 4 -26 -25 -237 -34 -246 -12 -12 -12 12 0 127 6 55 16 103 22 107 6 4 -2 12 -22 19 -26 9 -30 14 -21 25 15 18 24 18 40 -2z m-123 -226 c48 -23 54 -30 46 -51 -5 -13 -10 -14 -39 -1 -50 21 -163 18 -207 -5 -35 -17 -37 -17 -41 0 -7 24 23 49 85 70 58 20 89 18 156 -13z m28 -469 c39 -36 56 -76 56 -134 2 -169 -205 -230 -284 -85 -26 47 -21 155 7 176 10 8 15 14 10 14 -13 0 27 41 50 51 11 5 46 7 77 6 45 -3 64 -9 84 -28z m131 -37 c7 -8 10 -19 7 -23 -3 -5 -1 -9 4 -9 17 0 21 -41 10 -103 -10 -59 -28 -100 -28 -64 0 14 -44 137 -75 209 -6 15 -2 16 32 11 21 -3 43 -12 50 -21z m-455 -49 c-15 -38 -30 -71 -35 -74 -5 -3 -8 -9 -8 -13 3 -21 0 -36 -8 -36 -5 0 -11 30 -13 68 -5 61 -3 69 20 95 14 15 36 27 48 27 l22 0 -26 -67z m377 -317 c19 14 25 14 32 3 13 -21 12 -23 -21 -42 -16 -10 -30 -23 -30 -28 0 -6 -4 -8 -8 -5 -4 2 -9 -9 -10 -25 -2 -22 2 -29 16 -29 14 0 21 -12 29 -44 10 -43 9 -45 -13 -43 -13 1 -29 10 -37 21 -13 17 -15 14 -37 -40 -13 -33 -30 -65 -38 -72 -9 -6 -12 -12 -7 -12 4 0 0 -9 -10 -20 -35 -39 -107 -11 -117 45 -1 11 -9 26 -15 33 -12 12 -17 33 -15 54 1 3 -5 11 -13 18 -11 9 -16 7 -25 -9 -12 -23 -50 -29 -50 -7 0 44 14 76 32 76 17 0 19 4 13 23 -4 12 -7 30 -8 39 -1 9 -4 15 -8 12 -4 -2 -19 6 -33 18 -19 16 -23 27 -17 37 8 12 13 12 34 -2 l25 -16 7 39 c4 22 8 40 9 40 1 0 18 -9 39 -19 l37 -19 -28 -8 c-15 -4 -30 -7 -33 -6 -3 0 -6 -11 -5 -26 l1 -27 125 0 c120 0 125 1 128 21 2 16 -5 25 -27 34 l-31 12 33 22 32 22 10 -43 c10 -43 10 -44 34 -27z m-161 11 c2 -10 -3 -17 -12 -17 -18 0 -29 16 -21 31 9 14 29 6 33 -14z m-58 -11 c0 -16 -18 -31 -27 -22 -8 8 5 36 17 36 5 0 10 -6 10 -14z m108 -10 c3 -12 -1 -17 -10 -14 -7 3 -15 13 -16 22 -3 12 1 17 10 14 7 -3 15 -13 16 -22z m-148 -5 c0 -14 -18 -23 -31 -15 -8 4 -7 9 2 15 18 11 29 11 29 0z m198 -3 c-3 -7 -11 -13 -18 -13 -7 0 -15 6 -17 13 -3 7 4 12 17 12 13 0 20 -5 18 -12z m-430 -224 c37 -10 25 -23 -14 -14 -19 5 -38 11 -41 14 -8 8 26 7 55 0z m672 -3 c-8 -5 -35 -11 -60 -15 l-45 -6 35 14 c39 16 96 21 70 7z m-454 -113 l29 -30 -30 15 c-17 8 -38 24 -48 35 -18 20 -18 20 1 15 11 -2 33 -18 48 -35z m239 15 c-11 -8 -33 -21 -50 -30 l-30 -15 30 31 c16 17 35 29 42 27 7 -3 13 0 13 6 0 6 3 8 7 4 5 -4 -1 -14 -12 -23z"
//     truck = 'M180 1725 c0 -8 -21 -25 -47 -37 -27 -12 -58 -31 -70 -42 l-23 -20 0 -495 0 -496 23 -18 c12 -10 31 -24 42 -32 20 -13 20 -14 -2 -37 -21 -22 -23 -34 -23 -131 0 -65 -4 -107 -10 -107 -5 0 -10 -7 -10 -15 0 -8 5 -15 10 -15 6 0 10 -31 10 -74 0 -44 5 -78 11 -82 8 -4 7 -9 -2 -15 -38 -23 96 -89 181 -89 85 0 219 66 181 89 -9 6 -10 11 -2 15 6 4 11 38 11 82 0 43 4 74 10 74 6 0 10 7 10 15 0 8 -4 15 -10 15 -6 0 -10 43 -10 108 0 96 -2 108 -20 120 -30 18 -24 38 20 69 l40 28 0 491 0 491 -22 24 c-22 24 -86 59 -106 59 -5 0 -12 9 -15 20 -5 18 -14 20 -91 20 -70 0 -86 -3 -86 -15z m115 -101 c21 -8 35 -45 28 -72 -7 -29 -47 -44 -78 -30 -29 13 -38 36 -29 72 7 30 43 44 79 30z m-148 -726 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m250 0 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m-102 326 c21 -9 35 -46 28 -74 -12 -44 -93 -46 -108 -1 -17 54 27 95 80 75z m20 -436 c2 -2 6 -15 10 -30 10 -38 -26 -72 -68 -64 -37 8 -35 6 -43 43 -8 42 18 67 64 59 19 -3 36 -6 37 -8z m18 -245 c-36 -16 -102 -16 -130 0 -18 10 -7 12 68 12 80 0 87 -1 62 -12z m-203 -69 c0 -21 -4 -33 -10 -29 -5 3 -10 19 -10 36 0 16 5 29 10 29 6 0 10 -16 10 -36z m300 1 c0 -24 -5 -35 -15 -35 -15 0 -22 50 -8 63 15 15 23 5 23 -28z m-300 -87 c0 -24 -5 -50 -10 -58 -7 -11 -10 2 -10 43 0 31 4 57 10 57 6 0 10 -19 10 -42z m300 -20 c0 -43 -3 -58 -10 -48 -13 20 -23 87 -15 100 15 25 25 5 25 -52z m-201 -44 c31 -4 80 -2 108 4 59 10 70 5 75 -43 3 -29 2 -30 -57 -38 -58 -8 -199 -2 -229 9 -12 5 -12 11 2 45 11 28 20 38 30 34 9 -3 40 -8 71 -11z m147 -222 c-14 -15 -15 -13 -3 17 8 23 13 28 15 16 2 -10 -3 -25 -12 -33z m-213 18 c3 -11 1 -20 -4 -20 -5 0 -9 9 -9 20 0 11 2 20 4 20 2 0 6 -9 9 -20z m267 -25 c0 -2 -10 -10 -22 -16 -21 -11 -22 -11 -9 4 13 16 31 23 31 12z'
//     carIcon = {
//         path: this.car,
//         labelOrigin: [25, 50],
//         strokeColor: 'black',
//         strokeWeight: .10,
//         fillOpacity: 1,
//         fillColor: 'blue',
//         anchor: [10, 20], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
//     };
//     busIcon = {
//         path: this.car,
//         labelOrigin: [25, 50],
//         strokeColor: 'black',
//         strokeWeight: .10,
//         fillOpacity: 1,
//         fillColor: 'blue',
//         anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
//     };
//     bikeIcon = {
//         path: this.bike,
//         labelOrigin: [25, 50],
//         strokeColor: 'black',
//         strokeWeight: .10,
//         fillOpacity: 1,
//         anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
//     };

//     truckIcon = {
//         path: this.truck,
//         labelOrigin: [25, 50],
//         strokeColor: 'black',
//         strokeWeight: .10,
//         fillOpacity: 1,
//         anchor: [10, 20], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
//     };

//     userIcon = {
//         path: this.truck,
//         labelOrigin: [25, 50],
//         strokeColor: 'black',
//         strokeWeight: .10,
//         fillOpacity: 1,
//         anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
//     }

//     icons = {
//         "car": this.carIcon,
//         "bike": this.bikeIcon,
//         "truck": this.truckIcon,
//         "bus": this.busIcon,
//         "user": this.userIcon
//     }

//     ionViewDidLoad() {
//         // this.allData.map = GoogleMaps.create('map_canvas_single_device');
//     }

//     newMap() {

//         // this.geolocation.getCurrentPosition().then((position) => {
//         //   var pos = {
//         //     lat: position.coords.latitude,
//         //     lng: position.coords.longitude
//         //   };
//         //   map.setCameraTarget(pos);
//         // }).catch((error) => {
//         //   console.log('Error getting location', error);
//         // });
//         let mapOptions = {
//             camera: { zoom: 10 },
//             gestures: {
//                 rotate: false,
//                 tilt: false,
//                 // scroll: false
//             }
//         };
//         let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
//         return map;
//     }

//     refreshMe() {
//     //   this.ngOnDestroy();
//     //   this.ngOnInit();
//       this.ngOnDestroy();
//       this._io = io.connect('http://139.59.50.225/gps' , { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
//       this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
//       //var paramData = this.navParams.get("this._commonVar.paramData");
//       console.log("pdata", this._commonVar.paramData);
//       this.socketInit(this._commonVar.paramData);
//   }

//     ngOnInit() {
//         this._commonVar._eta = {
//             'duration': 'N/A',
//             'distance': 'N/A'
//         };
//         this._commonVar.paramData = [];
//         this._io = io.connect('http://139.59.50.225/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
//         this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
//         this.showShareBtn = true;

//         this._commonVar.paramData = this.navParams.get("data");
//         console.log('device data', this._commonVar.paramData);
//         if (this._commonVar.paramData.scheduleschools.trackRoute != undefined) {
//             this.routeId = this._commonVar.paramData.scheduleschools.trackRoute._id;
//             this.trackRoute();
//         }

//         this.temp(this._commonVar.paramData);
//         // this.showActionSheet = true;
//         this.getProfilestudent();

//         setInterval(() => {
//             if (localStorage.getItem("ETA") != null) {
//                 // console.log("estimated time here: ", JSON.parse(localStorage.getItem("ETA")));
//                 this._commonVar._eta = {};
//                 this._commonVar._eta = JSON.parse(localStorage.getItem("ETA"));
//             }
//         }, 1000)
//         this.socketInit(this._commonVar.paramData);
//     }

//     ngOnDestroy() {
//         let that = this;
//         for (var i = 0; i < that.socketChnl.length; i++)
//             that._io.removeAllListeners(that.socketChnl[i]);
//         that._io.on('disconnect', () => {
//             that._io.open();
//         })

//         if (localStorage.getItem('ETA') != null) {
//             localStorage.removeItem("ETA");
//         }


//     }

//     dialNumber(number) {
//         if (this.plt.is('android')) {
//             this.callNumber.callNumber(number, true)
//                 .then(res => console.log('Launched dialer!', res))
//                 .catch(err => console.log('Error launching dialer', err));
//         } else if (this.plt.is('ios')) {
//             window.open('tel:' + number, '_system');
//         }
//     }

//     trafficFunc() {
//         let that = this;
//         that.isEnabled = !that.isEnabled;
//         if (that.isEnabled == true) {
//             that.allData.map.setTrafficEnabled(true);
//         } else {
//             that.allData.map.setTrafficEnabled(false);
//         }
//     }


//     getProfilestudent() {
//         this.apiCall.getProfilestudentApi(this.userdetails._id)
//             .subscribe(data => {
//                 this.profilesStudent = data[0].data;
//                 debugger
//                 this.origin = {
//                     "lat": data[0].data[0].students.latLng.lat,
//                     "lng": data[0].data[0].students.latLng.lng
//                 }
//                 var icUrl;
//                 if (this.plt.is('android')) {
//                     icUrl = './assets/imgs/pin_school_alt.png';
//                 } else if (this.plt.is('ios')) {
//                     icUrl = 'www/assets/imgs/pin_school_alt.png';
//                 }
//                 console.log("origine points: ", this.origin)
//                 for (var i = 0; i < data[0].data.length; i++) {

//                     this.allData.map.addMarker({
//                         title: data[0].data[i].students.school.schoolName,
//                         icon: icUrl,
//                         position: {
//                             lat: data[0].data[i].students.school.latLng.lat,
//                             lng: data[0].data[i].students.school.latLng.lng
//                         }
//                     });
//                 }
//             }, err => {

//                 console.log(err);
//             })
//     }

//     onClickMainMenu(item) {
//         this.menuActive = !this.menuActive;
//       }

//       onClickMap(maptype) {
//         let that = this;
//         if (maptype == 'SATELLITE' || maptype == 'HYBRID') {
//           this.ngOnDestroy();
//           that.allData.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
//           //this.someFunc();
//         } else {
//           if (maptype == 'TERRAIN') {
//             this.ngOnDestroy();
//             that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
//             //this.someFunc();
//           } else {
//             if (maptype == 'NORMAL') {
//               this.ngOnDestroy();
//               that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
//               //this.someFunc();
//             }
//           }
//         }
//       }

//     trackRoute() {
//         let that = this;
//         console.log("getgroup");
//         //var baseURLp = 'http://www.thingslab.in/trackRoute/routepath/getRoutePathWithPoi?id=' +  this.routeId + '&user=' + this.userdetails._id ;
//         that.apiCall.startLoading().present();
//         that.apiCall.getTrackrouteCall(that.routeId, that.userdetails.supAdmin)
//             .subscribe(data => {
//                 that.apiCall.stopLoading();
//                 debugger
//                 this.routeData = data;
//                 that.mapData = [];
//                 for (var i = 0; i < that.routeData.routePath.length; i++) {
//                     that.mapData.push({ "lat": that.routeData.routePath[i].location.coordinates[1], "lng": that.routeData.routePath[i].location.coordinates[0] });
//                 }
//                 that.locations = [];

//                 for (var i = 0; i < that.routeData.poi.length; i++) {
//                     var arr = {
//                         lat: that.routeData.poi[i].poi.location.coordinates[1],
//                         lng: that.routeData.poi[i].poi.location.coordinates[0],
//                         stopName: that.routeData.poi[i].poi.poiname,
//                         sbUsers: that.routeData.poi[i].sbUsers[0]
//                     };

//                     that.locations.push(arr)
//                 }
//                 console.log('stop point', that.locations)
//                 console.log('point user id', that.userdetails._id)
//                 var icUrlPark, icUrlFlag;
//                 if (that.plt.is('android')) {
//                     icUrlFlag = './assets/imgs/flag.png';
//                     icUrlPark = './assets/imgs/park.png';
//                 } else if (that.plt.is('ios')) {
//                     icUrlFlag = 'www/assets/imgs/flag.png';
//                     icUrlPark = 'www/assets/imgs/park.png';
//                 }
//                 for (var i = 0; i < that.locations.length; i++) {

//                     if (that.locations[i].sbUsers = that.userdetails._id) {
//                         that.allData.map.addMarker({
//                             title: that.locations[i].stopName,
//                             position: that.locations[i],
//                             icon: icUrlFlag
//                         }).then((marker: Marker) => {
//                             marker.showInfoWindow();
//                         });
//                     } else {
//                         that.allData.map.addMarker({
//                             title: that.locations[i].stopName,
//                             position: that.locations[i],
//                             icon: icUrlPark
//                         }).then((marker: Marker) => {
//                             marker.showInfoWindow();
//                         });
//                     }
//                 }
//                 console.log("latlang.............", that.mapData)
//                 if (that.mapData.length > 1) {
//                     that.allData.map.addPolyline({
//                         points: that.mapData,
//                         color: 'blue',
//                         width: 4,
//                         geodesic: true
//                     })
//                 }
//             },
//                 err => {
//                     this.apiCall.stopLoading();
//                     console.log("error found=> " + err);
//                 });
//     }
//     debugger;
//     socketInit(pdata, center = false) {
//         debugger;
//         console.log("imei resend => ", pdata.scheduleschools.schoolVehicles.device.Device_ID)
//         this._io.emit('acc', pdata.scheduleschools.schoolVehicles.device.Device_ID);
//         this.socketChnl.push(pdata.scheduleschools.schoolVehicles.device.Device_ID + 'acc');
//         this._io.on(pdata.scheduleschools.schoolVehicles.device.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {
//             debugger
//             console.log("d4 data=> " + d4);
//             // var strStr;
//             // strStr = moment(new Date(d4.last_ping_on)).format('LLLL'); 
//             let that = this;
//             if (d4 != undefined)
//                 (function (data) {

//                     if (data == undefined) {
//                         return;
//                     }
//                     if (data._id != undefined && data.last_location != undefined) {

//                         var key = data._id;
//                         let ic = _.cloneDeep(that.icons[data.iconType]);
//                         if (!ic) {
//                             return;
//                         }
//                         ic.path = null;
//                         if (that.plt.is('android')) {
//                             ic.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';

//                         } else if (that.plt.is('ios')) {
//                             ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';

//                         }
//                         console.log("icon url => ", ic.url)
//                         that.last_ping_on = moment(new Date(data.last_ping_on)).format('LLL');
//                         console.log("last", that.last_ping_on);
//                         that.vehicle_speed = data.last_speed;
//                         that.todays_odo = data.today_odo;
//                         that.gpsTracking = data.gpsTracking;

//                         if (that.markObj != undefined) {
//                             that.markObj.remove();
//                             console.log("marker removed");
//                         }

//                         if (that.allData[key]) {
//                             that.socketSwitch[key] = data;
//                             that.allData[key].mark.setIcon(ic);
//                             that.allData[key].mark.setPosition(that.allData[key].poly[1]);
//                             console.log("marker added on if condition")
//                             console.log("marker position: ", JSON.stringify(that.allData[key].mark.getPosition()))
//                             var temp = _.cloneDeep(that.allData[key].poly[1]);
//                             that.allData[key].poly[0] = _.cloneDeep(temp);
//                             that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
//                             debugger
//                             that.destination = {};
//                             that.destination = {
//                                 "lat": data.last_location.lat,
//                                 "lng": data.last_location.long
//                             }
//                             console.log("destination in if: ", JSON.stringify(that.destination))
//                             if (that.origin != undefined && that.destination != undefined) {
//                                 if ((that.origin.lat != 0 || that.origin.lng != 0) && (that.destination.lat != 0 || that.destination.lng != 0)) {
//                                     that.getTravelDetails(that.origin, that.destination);
//                                 }
//                             }

//                             // var speed = data.status == "RUNNING" ? data.last_speed : 0;
//                             that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].poly, 50, 10, center, data._id, that);
//                         }
//                         else {
//                             that.allData[key] = {};
//                             that.socketSwitch[key] = data;
//                             that.allData[key].poly = [];
//                             // if (data.sec_last_location) {
//                             //     that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
//                             // } else {
//                             //     that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
//                             // }
//                             that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
//                             if (data.last_location != undefined) {
//                                 if (that.rememberMark) {
//                                     that.rememberMark.remove();
//                                 }
//                                 that.allData.map.addMarker({
//                                     title: data.Device_Name,
//                                     position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
//                                     icon: ic,
//                                 }).then((marker: Marker) => {
//                                     console.log("marker added on else condition")

//                                     that.allData[key].mark = marker;
//                                     that.destination = {};
//                                     that.destination = {
//                                         "lat": marker.getPosition().lat,
//                                         "lng": marker.getPosition().lng
//                                     }
//                                     console.log("destination: ", JSON.stringify(that.destination))
//                                     // debugger
//                                     if (that.origin != undefined && that.destination != undefined) {
//                                         if ((that.origin.lat != 0 || that.origin.lng != 0) && (that.destination.lat != 0 || that.destination.lng != 0)) {
//                                             that.getTravelDetails(that.origin, that.destination)
//                                         }
//                                     }

//                                     marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
//                                         .subscribe(e => {
//                                             that.liveVehicleName = data.Device_Name;
//                                         });
//                                     // var speed = data.status == "RUNNING" ? data.last_speed : 0;
//                                     that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].poly, 50, 10, center, data._id, that);
//                                 })

//                             }
//                         }
//                     }
//                 })(d4)
//         })
//     }

//     liveTrack(map, mark, coords, speed, delay, center, id, that) {

//         var target = 0;

//         clearTimeout(that.ongoingGoToPoint[id]);
//         clearTimeout(that.ongoingMoveMarker[id]);
//         if (center) {
//             map.setCameraTarget(coords[0]);
//         }
//         function _goToPoint() {
//             console.log('mark', mark);
//             var lat = mark.getPosition().lat;
//             var lng = mark.getPosition().lng;

//             var step = (speed * 1000 * delay) / 3600000; // in meters
//             if (coords[target])
//                 var dest = new LatLng(coords[target].lat, coords[target].lng);
//             else
//                 return;

//             console.log("dest point: ", JSON.stringify(dest));
//             var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
//             console.log("dest point: ", JSON.stringify(dest));
//             var numStep = distance / step;
//             console.log("dest point: ", JSON.stringify(dest));
//             var i = 0;
//             var deltaLat = (coords[target].lat - lat) / numStep;
//             var deltaLng = (coords[target].lng - lng) / numStep;
//             function changeMarker(mark, head) {
//                 mark.setRotation(head);
//             }
//             function _moveMarker() {
//                 lat += deltaLat;
//                 lng += deltaLng;
//                 i += step;
//                 var head = 0;
//                 if (i < distance) {
//                     head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
//                     head = head < 0 ? (360 + head) : head;
//                     console.log("head 1: ", head)
//                     console.log("isNaN head 1: ", isNaN(head))
//                     // if ((head != 0) || (head == NaN)) {
//                     //     changeMarker(mark, head);
//                     // }
//                     if (head !== 0) {
//                         if (isNaN(head) == false) {
//                             console.log("1 inside head")
//                             changeMarker(mark, head);
//                         }
//                     }

//                     map.setCameraTarget(new LatLng(lat, lng));
//                     that.latlngCenter = new LatLng(lat, lng);
//                     mark.setPosition(new LatLng(lat, lng));
//                     that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
//                 }
//                 else {
//                     head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
//                     head = head < 0 ? (360 + head) : head;
//                     console.log("head 2: ", head)
//                     console.log("isNaN head 2: ", isNaN(head))
//                     if (head !== 0) {
//                         if (isNaN(head) == false) {
//                             console.log("2 inside head")
//                             changeMarker(mark, head);
//                         }
//                     }
//                     // if ((head !== 0) && (head !== NaN)) {
//                     //     console.log("2 inside head")
//                     //     changeMarker(mark, head);
//                     // }
//                     map.setCameraTarget(dest);
//                     that.latlngCenter = dest;
//                     mark.setPosition(dest);
//                     target++;
//                     that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
//                 }
//             }
//             _moveMarker();
//         }
//         _goToPoint();
//     }

//     getTravelDetails(ori, dest) {
//         let that = this;
//         that.service.getDistanceMatrix({
//             origins: [new google.maps.LatLng(ori.lat, ori.lng)],
//             destinations: [new google.maps.LatLng(dest.lat, dest.lng)],
//             travelMode: 'DRIVING'
//         }, that.callback);
//     }

//     callback(response, status) {
//         let travelDetailsObject;
//         if (status == 'OK') {
//             var origins = response.originAddresses;
//             for (var i = 0; i < origins.length; i++) {
//                 var results = response.rows[i].elements;
//                 for (var j = 0; j < results.length; j++) {
//                     var element = results[j];
//                     var distance = element.distance.text;
//                     var duration = element.duration.text;
//                     travelDetailsObject = {
//                         distance: distance,
//                         duration: duration
//                     }
//                 }
//             }
//             localStorage.setItem("ETA", JSON.stringify(travelDetailsObject));
//         }
//     }

//     zoomin() {
//         let that = this;
//         that.allData.map.moveCameraZoomIn();
//     }
//     zoomout() {
//         let that = this;
//         that.allData.map.animateCameraZoomOut();
//     }

//     temp(data) {

//         console.log('data', data);
//         let that = this;
//         that.showShareBtn = true;
//         that.liveDataShare = data;

//         if (that.allData.map != undefined) {
//             that.allData.map.remove();
//         }

//         for (var i = 0; i < that.socketChnl.length; i++)
//             that._io.removeAllListeners(that.socketChnl[i]);
//         that.allData = {};
//         that.socketChnl = [];
//         that.socketSwitch = {};
//         debugger;
//         if (data) {
//             console.log('data for live', data)
//             console.log("last_loaction=> " + data.scheduleschools.schoolVehicles.device.last_location)
//             // if (data.last_location) {
//             if (data.scheduleschools.schoolVehicles.device.last_location) {
//                 let mapOptions = {
//                     backgroundColor: 'white',
//                     controls: {
//                         compass: true,
//                         zoom: true,
//                     },
//                     gestures: {
//                         rotate: false,
//                         tilt: false,
//                         // scroll: false
//                     },
//                     camera: {
//                         target: {
//                             lat: data.scheduleschools.schoolVehicles.device.last_location['lat'],
//                             lng: data.scheduleschools.schoolVehicles.device.last_location['long']
//                         },
//                         zoom: 18,
//                         tilt: 30
//                     }
//                 }
//                 let map = GoogleMaps.create('map_canvas_single_device', mapOptions);

//                 // map.animateCamera({
//                 //     target: { lat: 20.5937, lng: 78.9629 },
//                 //     zoom: 15,
//                 //     duration: 3000,
//                 //     padding: 0  // default = 20px
//                 // });
//                 // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
//                 // that.allData.map = that.newMap();
//                 // map.setCameraTarget({ lat: data.scheduleschools.schoolVehicles.device.last_location['lat'], lng: data.scheduleschools.schoolVehicles.device.last_location['long'] });
//                 that.allData.map = map;
//                 var iconIc;
//                 if (that.plt.is('android')) {
//                     iconIc = './assets/imgs/vehicles/runningbus.png';

//                 } else if (that.plt.is('ios')) {
//                     iconIc = 'www/assets/imgs/vehicles/runningbus.png';

//                 }
//                 // that.allData.map.setCameraTarget({ lat: data.scheduleschools.schoolVehicles.device.last_location['lat'], lng: data.scheduleschools.schoolVehicles.device.last_location['long'] })
//                 that.allData.map.addMarker({
//                     position: { lat: data.scheduleschools.schoolVehicles.device.last_location['lat'], lng: data.scheduleschools.schoolVehicles.device.last_location['long'] },
//                     icon: iconIc
//                 }).then((marker: Marker) => {
//                     console.log("success in placing marker=> ", marker)
//                     that.markObj = marker;
//                     that.socketInit(data);
//                 })
//                 // that.socketInit(data);
//             } else {
//                 that.allData.map = that.newMap();
//                 that.socketInit(data);
//             }
//         }
//     }



//new code
navigateButtonColor: string = '#d80622'; //Default Color
navColor: string = '#fff';

policeButtonColor: string = '#d80622';
policeColor: string = '#fff';

petrolButtonColor: string = "#d80622";
petrolColor: string = '#fff';

shouldBounce = true;
dockedHeight = 80;
distanceTop = 200;
// drawerState = DrawerState.Docked;
// states = DrawerState;
minimumHeight = 50;
transition = '0.85s ease-in-out';

ongoingGoToPoint: any = {};
ongoingMoveMarker: any = {};

data: any = {};
_io: any;
socketSwitch: any = {};
socketChnl: any = [];
socketData: any = {};
allData: any = {};
userdetails: any;
showBtn: boolean;
SelectVehicle: string;
selectedVehicle: any;
titleText: any;
portstemp: any;
gpsTracking: any;
power: any;
currentFuel: any;
last_ACC: any;
today_running: string;
today_stopped: string;
timeAtLastStop: string;
distFromLastStop: any;
lastStoppedAt: string;
fuel: any;
total_odo: any;
todays_odo: any;
vehicle_speed: any;
liveVehicleName: any;
onClickShow: boolean;
address: any;
resToken: string;
liveDataShare: any;
isEnabled: boolean = false;
showMenuBtn: boolean = false;
latlngCenter: any;
mapHideTraffic: boolean = false;
mapData: any = [];
last_ping_on: any;
geodata: any = [];
geoShape: any = [];
generalPolygon: any;
locations: any = [];
acModel: any;
locationEndAddress: any;
tempaddress: any;
mapKey: string;
shwBckBtn: boolean = false;
recenterMeLat: any;
recenterMeLng: any;
menuActive: boolean;
deviceDeatils: any = {};
impkey: any;
showaddpoibtn: boolean = false;
isSuperAdmin: boolean;
isDealer: boolean;
zoomLevel: number = 18;
voltage: number;
temporaryMarker: any;
temporaryInterval: any;
circleObj: Circle;
circleTimer: any;
tempMarkArray: any = [];
tempreture: number;
door: number;

constructor(
  //private socialSharing: SocialSharing,
  public alertCtrl: AlertController,
  private viewCtrl: ViewController,
  //private translate: TranslateService,
  private toastCtrl: ToastController,
  //private geocoderApi: GeocoderProvider,
  public navCtrl: NavController,
  public navParams: NavParams,
  public apiCall: ApiCallerProvider,
  public actionSheetCtrl: ActionSheetController,
  public elementRef: ElementRef,
  public modalCtrl: ModalController,
  public plt: Platform,
  private callNumber: CallNumber
  // private launchNavigator: LaunchNavigator,
) {
    this.newMap();
  var selectedMapKey;
  if (localStorage.getItem('MAP_KEY') != null) {
    selectedMapKey = localStorage.getItem('MAP_KEY');
    if (selectedMapKey ==('Hybrid')) {
      this.mapKey = 'MAP_TYPE_HYBRID';
    } else if (selectedMapKey == ('Normal')) {
      this.mapKey = 'MAP_TYPE_NORMAL';
    } else if (selectedMapKey == ('Terrain')) {
      this.mapKey = 'MAP_TYPE_TERRAIN';
    } else if (selectedMapKey == ('Satellite')) {
      this.mapKey = 'MAP_TYPE_HYBRID';
    }
  } else {
    this.mapKey = 'MAP_TYPE_NORMAL';
  }
  this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
  console.log("user details=> " + JSON.stringify(this.userdetails));
  this.menuActive = false;
}

car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';

carIcon = {
  path: this.car,
  labelOrigin: [25, 50],
  strokeColor: 'black',
  strokeWeight: .10,
  fillOpacity: 1,
  fillColor: 'blue',
  anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
};

icons = {
  "car": this.carIcon,
  "bike": this.carIcon,
  "truck": this.carIcon,
  "bus": this.carIcon,
  "user": this.carIcon,
  "jcb": this.carIcon,
  "tractor": this.carIcon,
  "ambulance": this.carIcon
}
goBack() {
  this.navCtrl.pop({ animate: true, direction: 'forward' });
}

onClickMainMenu(item) {
  this.menuActive = !this.menuActive;
}

refreshMe() {
  this.ngOnDestroy();
  this.ngOnInit();
}

refreshMe1() {
  let that = this;
  this.ngOnDestroy();
  this._io = io.connect('http://139.59.50.225/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
  this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
  var paramData = that.data;
  this.socketInit(paramData);
}

resumeListener: Subscription = new Subscription();
ionViewWillEnter() {
  if (this.plt.is('ios')) {
    this.shwBckBtn = true;
    this.viewCtrl.showBackButton(false);
  }

  this.plt.ready().then(() => {
    this.resumeListener = this.plt.resume.subscribe(() => {
      var today, Christmas;
      today = new Date();
      Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
      var diffMs = (today - Christmas); // milliseconds between now & Christmas
      var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
      if (diffMins >= 5) {
        localStorage.removeItem("backgroundModeTime");
        this.alertCtrl.create({
          message: ("Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?"),
          buttons: [
            {
              text: ('YES PROCEED'),
              handler: () => {
                // this.getdevices();
              }
            },
            {
              text: ('Back'),
              handler: () => {
                this.navCtrl.setRoot('DashboardPage');
              }
            }
          ]
        }).present();
      }
    })
  })
}

ionViewWillLeave() {
  this.plt.ready().then(() => {
    this.resumeListener.unsubscribe();
  })
}

ngOnInit() {
  if (localStorage.getItem("entered_userdevices_123")) {
    localStorage.removeItem("entered_userdevices_123")
  }
  if (localStorage.getItem("entered_userdevices")) {
    localStorage.removeItem("entered_userdevices")
  }
  if (localStorage.getItem("CLICKED_ALL") != null) {
    this.showMenuBtn = true;
  }
  if (localStorage.getItem("SCREEN") != null) {
    if (localStorage.getItem("SCREEN") === 'live') {
      this.showMenuBtn = true;
    }
  }
  if (this.socketChnl.length > 0) {
    this.ngOnDestroy();
  }
  ////////////////
  this.mapHideTraffic = false;
  // this.drawerHidden = true;
  //this.drawerState = DrawerState.Bottom;
  this.onClickShow = false;

  this.showBtn = false;
  this.SelectVehicle = "Select Vehicle";
  this.selectedVehicle = undefined;
  let that = this;
  this._io = io.connect('http://139.59.50.225/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
  this._io.on('connect', function (data) {
    console.log('Ignition IO Connected', data);

    if (localStorage.getItem("SocketReconnectingStart") !== null) {
      localStorage.removeItem("SocketReconnectingStart");
    }
    if (localStorage.getItem("entered_userdevices_123") === null) {
      localStorage.setItem("entered_userdevices_123", "true");
      that.userDevices();
    }
  });
  ////////////////
  this._io.on('connect_failed', function () {
    console.log("Sorry, there seems to be an issue with the connection!");
  });
  this._io.on('error', function () {
    console.log("socket io got error!");
  })
  this._io.on('reconnecting', function () {
    console.log("socket io trying to reconnect!");

    localStorage.setItem("SocketReconnectingStart", "true");

    if (localStorage.getItem("entered_userdevices") === null) {

      if (localStorage.getItem("entered_userdevices_123") === null) {
        localStorage.setItem("entered_userdevices", "true");
        that.userDevices();
      }

    }
  })
  this._io.on('reconnect_failed', function () {
    console.log("reconnecting failed!");
  })
  this._io.on('disconnect', function () {
    console.log('client socket-io disconnect!')
  });

  // this.userDevices();
}

ngOnDestroy() {
  localStorage.removeItem("CLICKED_ALL");

  for (var i = 0; i < this.socketChnl.length; i++)
    this._io.removeAllListeners(this.socketChnl[i]);
  this._io.on('disconnect', () => {
    this._io.open();
  })
}

userDevices() {

  var baseURLp;
  let that = this;
  if (that.allData.map != undefined) {
    that.allData.map.remove();
    that.allData.map = that.newMap();
  } else {
    that.allData.map = that.newMap();
  }

  baseURLp = this.apiCall.link + 'devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;

  if (this.userdetails.isSuperAdmin == true) {
    baseURLp += '&supAdmin=' + this.userdetails._id;
    this.isSuperAdmin = true;
  } else {
    if (this.userdetails.isDealer == true) {
      baseURLp += '&dealer=' + this.userdetails._id;
      this.isDealer = true;
    }
  }

  that.apiCall.startLoading().present();
    that.apiCall.getdevicesApi(baseURLp)
    .subscribe(resp => {
      that.apiCall.stopLoading();
      that.portstemp = resp.devices;
      // console.log("list of vehicles :", that.portstemp)
      that.mapData = [];
      that.mapData = resp.devices.map(function (d) {
        if (d.last_location !== undefined) {
          return { lat: d.last_location['lat'], lng: d.last_location['long'] };
        }
      });

      var dummyData;
      dummyData = resp.devices.map(function (d) {
        if (d.last_location === undefined)
          return;
        else {
          return {
            "position": {
              "lat": d.last_location['lat'],
              "lng": d.last_location['long']
            },
            "name": d.Device_Name,
            "icon": {
              "url": that.getIconUrl(d),
              "size": {
                "width": 20,
                "height": 40
              }
            }
          };
        }
      });

      dummyData = dummyData.filter(function (element) {
        return element !== undefined;
      });

      console.log("dummy data: ", dummyData);
      // console.log("dummy data: ", this.dummyData());

    //   let bounds = new LatLngBounds(that.mapData);
    //   that.allData.map.moveCamera({
    //     target: bounds,
    //     zoom: 10
    //   })

      // this.innerFunc(that.portstemp);
      // this.addCluster(this.dummyData());
      if (that.isSuperAdmin || that.isDealer) {
        for (var t = 0; t < dummyData.length; t++) {
          console.log("check position: ", dummyData[t].position);
        }
        // that.addCluster(this.dummyData());
        that.addCluster(dummyData);

      } else {
        for (var i = 0; i < resp.devices.length; i++) {
          if (resp.devices[i].status != "Expired") {
            if (localStorage.getItem("SocketReconnectingStart")) {
              that.weDidntGetPing(resp.devices[i]);
            } else {
              that.socketInit(resp.devices[i]);
            }
          }
        }
        // that.ngOnDestroy();
        // that.portstemp = resp.devices;
      }

    },
      err => {
        that.apiCall.stopLoading();
        console.log(err);
      });
}
newMap() {
  let mapOptions = {
    controls: {
      zoom: false,
      myLocation: true,
      myLocationButton: false,
    },
    mapTypeControlOptions: {
      mapTypeIds: [GoogleMapsMapTypeId.ROADMAP, 'map_style']
    },
    gestures: {
      rotate: false,
      tilt: false
    },
    mapType: this.mapKey
  }
  debugger;
  let map = GoogleMaps.create('map_canvas', mapOptions);
  if (this.plt.is('android')) {
    // map.animateCamera({
    //   target: { lat: 20.5937, lng: 78.9629 },
    //   duration: 500,
    //   padding: 10,  // default = 20px
    // })
    map.setPadding(20, 20, 20, 20);
  }
  return map;
}

// navigateFromCurrentLoc() {
//   // this.ngOnDestroy();
//   if (this.navigateButtonColor == '#d80622') {
//     this.navigateButtonColor = '#f4f4f4';
//     this.navColor = '#d80622';
//     // this.apiCall.startLoading().present();
//     this.allData.map.getMyLocation().then((location) => {
//       var myLocCoords = new LatLng(location.latLng.lat, location.latLng.lng);
//       var vehLocCoords = new LatLng(this.recenterMeLat, this.recenterMeLng);
//       this.calcRoute(myLocCoords, vehLocCoords);
//     });
//   } else {
//     if (this.navigateButtonColor == '#f4f4f4') {
//       this.navigateButtonColor = '#d80622';
//       this.navColor = '#fff';
//       if (this.polyLines.length !== 0) {
//         // this.polyLines.remove();
//         for (var w = 0; w < this.polyLines.length; w++) {
//           this.polyLines[w].remove();
//         }
//       }
//     }
//   }
// }
// navigateFromCurrentLoc() {
//   // this.allData.map.getMyLocation().then((location) => {
//   //   // var myLocCoords = new LatLng(location.latLng.lat, location.latLng.lng);
//   //   // var vehLocCoords = new LatLng(this.recenterMeLat, this.recenterMeLng);
//   //   let options: LaunchNavigatorOptions = {
//   //     start: [location.latLng.lat, location.latLng.lng],
//   //     // destinationName: this.recenterMeLat + "," + this.recenterMeLng,
//   //     // app: LaunchNavigator.APPS.UBER
//   //   }

//   //   this.launchNavigator.navigate([this.recenterMeLat, this.recenterMeLng], options)
//   //     .then(
//   //       success => console.log('Launched navigator'),
//   //       error => console.log('Error launching navigator', error)
//   //     );
//   // });
//   window.open("https://www.google.com/maps/dir/?api=1&destination=" + this.recenterMeLat + "," + this.recenterMeLng + "&travelmode=driving", "_blank");

// }

    dialNumber(number) {
        if (this.plt.is('android')) {
            this.callNumber.callNumber(number, true)
                .then(res => console.log('Launched dialer!', res))
                .catch(err => console.log('Error launching dialer', err));
        } else if (this.plt.is('ios')) {
            window.open('tel:' + number, '_system');
        }
    }

// calcRoute(start, end) {
//   this.allData.AIR_PORTS = [];
//   var directionsService = new google.maps.DirectionsService();
//   let that = this;
//   var request = {
//     origin: start,
//     destination: end,
//     optimizeWaypoints: true,
//     travelMode: google.maps.TravelMode.DRIVING
//   };

//   directionsService.route(request, function (response, status) {

//     if (status == google.maps.DirectionsStatus.OK) {
//       var path = new google.maps.MVCArray();
//       for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
//         path.push(response.routes[0].overview_path[i]);

//         that.allData.AIR_PORTS.push({
//           lat: path.g[i].lat(), lng: path.g[i].lng()
//         });
//         // debugger
//         if (that.allData.AIR_PORTS.length > 1) {
//           that.allData.map.addPolyline({
//             'points': that.allData.AIR_PORTS,
//             'color': '#4aa9d5',
//             'width': 4,
//             'geodesic': true,
//           }).then((poly: Polyline) => {
//             that.polyLines.push(poly);
//             that.getTravelDetails(start, end);

//             //   that.showBtn = true;
//           })
//         }
//       }

//     }
//   });
// }

_id: any;
expectation: any = {};
polyLines: any[] = [];
marksArray: any[] = [];
service = new google.maps.DistanceMatrixService();

getTravelDetails(source, dest) {
  let that = this;
  this._id = setInterval(() => {
    if (localStorage.getItem("livepagetravelDetailsObject") != null) {
      if (that.expectation.distance == undefined && that.expectation.duration == undefined) {
        that.expectation = JSON.parse(localStorage.getItem("livepagetravelDetailsObject"));
        console.log("expectation: ", that.expectation)
      } else {
        clearInterval(this._id);
      }
    }
  }, 3000)
  that.service.getDistanceMatrix({
    origins: [source],
    destinations: [dest],
    travelMode: 'DRIVING'
  }, that.callback);
  // that.apiCall.stopLoading();
}

callback(response, status) {
  let travelDetailsObject;
  if (status == 'OK') {
    var origins = response.originAddresses;
    for (var i = 0; i < origins.length; i++) {
      var results = response.rows[i].elements;
      for (var j = 0; j < results.length; j++) {
        var element = results[j];
        var distance = element.distance.text;
        var duration = element.duration.text;
        travelDetailsObject = {
          distance: distance,
          duration: duration
        }
      }
    }
    localStorage.setItem("livepagetravelDetailsObject", JSON.stringify(travelDetailsObject));
  }
}

onClickMap(maptype) {
  let that = this;
  if (maptype == 'SATELLITE' || maptype == 'HYBRID') {
    // this.ngOnDestroy();
    that.allData.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
    // this.someFunc();
  } else {
    if (maptype == 'TERRAIN') {
      // this.ngOnDestroy();
      that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
      // this.someFunc();
    } else {
      if (maptype == 'NORMAL') {
        // this.ngOnDestroy();
        that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
        // this.someFunc();
      }
    }
  }
}

someFunc() {
  var runningDevices = [];
  let that = this;
  that._io = undefined;
  that._io = io.connect('http://139.59.50.225/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
  that._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
  // debugger
  // var paramData = that.data;
  // this.socketInit(paramData);
  if (that.isSuperAdmin || that.isDealer) {

  } else {
    if (that.selectedVehicle === undefined) {
      // setTimeout(() => {
      //   for (var i = 0; i < that.portstemp.length; i++) {
      //     if (that.portstemp[i].status != "Expired") {
      //       // debugger
      //       if (that.portstemp[i].status === "RUNNING") {
      //         if (runningDevices.length <= 5) {
      //           runningDevices.push(that.portstemp[i]);
      //         }
      //       }
      //     }
      //   }
      //   for (var j = 0; j < runningDevices.length; j++) {
      //     console.log("runningDevices[i]: ", runningDevices[j]);
      //     that.socketInit(runningDevices[j]);
      //   }
      // }, 3000);
    } else if (that.selectedVehicle !== undefined) {
      that.socketInit(that.selectedVehicle);
    }

  }
}

settings() {
  let that = this;
  let profileModal = this.modalCtrl.create('DeviceSettingsPage', {
    param: that.deviceDeatils
  });
  profileModal.present();

  profileModal.onDidDismiss(() => {
    for (var i = 0; i < that.socketChnl.length; i++)
      that._io.removeAllListeners(that.socketChnl[i]);

    var paramData = this.navParams.get("device");
    this.temp(paramData);
  })
}

// addPOI() {
//   let that = this;
//   let modal = this.modalCtrl.create(PoiPage, {
//     param1: that.allData[that.impkey]
//   });
//   modal.onDidDismiss((data) => {
//     console.log(data)
//     let that = this;
//     that.showaddpoibtn = false;
//   });
//   modal.present();
// }

onSelectMapOption(type) {
  let that = this;
  if (type == 'locateme') {
    that.allData.map.setCameraTarget(that.latlngCenter);
  } else {
    if (type == 'mapHideTraffic') {
      // this.ngOnDestroy();
      that.mapHideTraffic = !that.mapHideTraffic;
      if (that.mapHideTraffic) {
        that.allData.map.setTrafficEnabled(true);
        // this.someFunc();
      } else {
        that.allData.map.setTrafficEnabled(false);
        // this.someFunc();
      }
    } else {
      if (type == 'showGeofence') {
        console.log("Show Geofence")
        if (that.generalPolygon != undefined) {
          that.generalPolygon.remove();
          that.generalPolygon = undefined;
        } else {
          //that.callGeofence();
        }
      }
    }
  }
}

// callGeofence() {
//   this.geoShape = [];
//   this.apiCall.startLoading().present();
//   this.apiCall.getGeofenceCall(this.userdetails._id)
//     .subscribe(data => {
//       this.apiCall.stopLoading();
//       console.log("geofence data=> " + data.length)
//       if (data.length > 0) {
//         this.geoShape = data.map(function (d) {
//           return d.geofence.coordinates[0];
//         });
//         for (var g = 0; g < this.geoShape.length; g++) {
//           for (var v = 0; v < this.geoShape[g].length; v++) {
//             this.geoShape[g][v] = this.geoShape[g][v].reverse()
//           }
//         }

//         for (var t = 0; t < this.geoShape.length; t++) {
//           this.drawPolygon(this.geoShape[t])
//         }
//       } else {
//         let alert = this.alertCtrl.create({
//           message: 'No gofence found..!!',
//           buttons: ['OK']
//         });
//         alert.present();
//       }
//     },
//       err => {
//         this.apiCall.stopLoading();
//         console.log(err)
//       });
// }

// drawPolygon(polyData) {
//   let that = this;
//   that.geodata = [];
//   that.geodata = polyData.map(function (d) {
//     return { lat: d[0], lng: d[1] }
//   });
//   console.log("geodata=> ", that.geodata)

//   let bounds = new LatLngBounds(that.geodata);
//   that.allData.map.moveCamera({
//   target: bounds
//   });
//   let GORYOKAKU_POINTS: ILatLng[] = that.geodata;
//   console.log("GORYOKAKU_POINTS=> ", GORYOKAKU_POINTS)
//   that.allData.map.addPolygon({
//     'points': GORYOKAKU_POINTS,
//     'strokeColor': '#AA00FF',
//     'fillColor': '#00FFAA',
//     'strokeWidth': 2
//   }).then((polygon: Polygon) => {
//     // polygon.on(GoogleMapsEvent.POLYGON_CLICK).subscribe((param) => {
//     //   console.log("polygon data=> " + param)
//     //   console.log("polygon data=> " + param[1])
//     // })
//     this.generalPolygon = polygon;
//   });
// }

gotoAll() {
  this.navCtrl.setRoot('LivePage');
  localStorage.setItem("CLICKED_ALL", "CLICKED_ALL");
}

// shareLive() {
//   var data = {
//     id: this.liveDataShare._id,
//     imei: this.liveDataShare.Device_ID,
//     sh: this.userdetails._id,
//     ttl: 60 // set to 1 hour by default
//   };
//   this.apiCall.startLoading().present();
//   this.apiCall.shareLivetrackCall(data)
//     .subscribe(data => {
//       this.apiCall.stopLoading();
//       this.resToken = data.t;
//       this.liveShare();
//     },
//       err => {
//         this.apiCall.stopLoading();
//         console.log(err);
//       });
// }

// getPOIs() {
//   this.allData._poiData = [];
//   const _burl = this.apiCall.mainUrl + "poi/getPois?user=" + this.userdetails._id;
//   this.apiCall.startLoading().present();
//   this.apiCall.getSOSReportAPI(_burl)
//     .subscribe(data => {
//       this.apiCall.stopLoading();
//       this.allData._poiData = data;

//       this.plotPOI();
//     });
// }

plotPOI() {
  let toast = this.toastCtrl.create({
    message: "Plotting POI's please wait...",
    position: 'middle'
  });
  var icurl;
  if (this.plt.is('android')) {
    icurl = './assets/imgs/poiFlag.png';
  } else if (this.plt.is('ios')) {
    icurl = 'www/assets/imgs/poiFlag.png';

  }
  for (var v = 0; v < this.allData._poiData.length; v++) {
    toast.present();
    if (this.allData._poiData[v].poi.location.coordinates !== undefined) {
      this.allData.map.addMarker({
        title: this.allData._poiData[v].poi.poiname,
        position: {
          lat: this.allData._poiData[v].poi.location.coordinates[1],
          lng: this.allData._poiData[v].poi.location.coordinates[0]
        },
        icon: {
          url: icurl,
          size: {
            width: 20,
            height: 20
          }
        },
        animation: GoogleMapsAnimation.BOUNCE
      })
    }
  }
  toast.dismiss();
}

// liveShare() {
//   let that = this;
//   var link = this.apiCall.mainUrl + "share/liveShare?t=" + that.resToken;
//   that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
// }

zoomin() {
  let that = this;
  that.allData.map.animateCameraZoomIn();
}
zoomout() {
  let that = this;
  that.allData.map.animateCameraZoomOut();
}

getIconUrl(data) {
  let that = this;
  var iconUrl;
  if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0) {
    if (that.plt.is('ios')) {
      iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
    } else if (that.plt.is('android')) {
      iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
    }
  } else {
    if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0) {
      if (that.plt.is('ios')) {
        iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
      } else if (that.plt.is('android')) {
        iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
      }
    } else {
      if (that.plt.is('ios')) {
        iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
      } else if (that.plt.is('android')) {
        iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
      }
    }
  }
  return iconUrl;
}

parseMillisecondsIntoReadableTime(milliseconds) {
  //Get hours from milliseconds
  var hours = milliseconds / (1000 * 60 * 60);
  var absoluteHours = Math.floor(hours);
  var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

  //Get remainder from hours and convert to minutes
  var minutes = (hours - absoluteHours) * 60;
  var absoluteMinutes = Math.floor(minutes);
  var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

  //Get remainder from minutes and convert to seconds
  var seconds = (minutes - absoluteMinutes) * 60;
  var absoluteSeconds = Math.floor(seconds);
  var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

  // return h + ':' + m;
  return h + ':' + m + ':' + s;
}
otherValues(data) {
  let that = this;
  that.vehicle_speed = data.last_speed;
  that.todays_odo = data.today_odo;
  that.total_odo = data.total_odo;
  if (that.userdetails.fuel_unit == 'LITRE') {
    that.fuel = data.currentFuel;
  } else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
    that.fuel = data.fuel_percent;
  } else {
    that.fuel = data.currentFuel;
  }

  that.voltage = data.battery;

  if (data.last_location) {
    that.getAddress(data.last_location);
  }

  that.last_ping_on = data.last_ping_on;

  var tempvar = new Date(data.lastStoppedAt);
  if (data.lastStoppedAt != null) {
    var fd = tempvar.getTime();
    var td = new Date().getTime();
    var time_difference = td - fd;
    var total_min = time_difference / 60000;
    var hours = total_min / 60
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    that.lastStoppedAt = rhours + ':' + rminutes;
  } else {
    that.lastStoppedAt = '00' + ':' + '00';
  }

  that.distFromLastStop = data.distFromLastStop;
  if (!isNaN(data.timeAtLastStop)) {
    that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
  } else {
    that.timeAtLastStop = '00:00:00';
  }

  that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
  that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
  that.last_ACC = data.last_ACC;
  that.acModel = data.ac;
  that.currentFuel = data.currentFuel;
  that.power = data.power;
  that.gpsTracking = data.gpsTracking;
  that.tempreture = data.temp;
  that.door = data.door;
  that.recenterMeLat = data.last_location.lat;
  that.recenterMeLng = data.last_location.long;
}
weDidntGetPing(data) {
  let that = this;
  if (data._id != undefined && data.last_location != undefined) {
    that.otherValues(data);
    if (data.last_location != undefined) {

      that.allData.map.addMarker({
        title: data.Device_Name,
        position: { lat: data.last_location.lat, lng: data.last_location.long },
        icon: {
          url: that.getIconUrl(data),
          size: {
            width: 20,
            height: 40
          }
        },
      }).then((marker: Marker) => {
        if (that.selectedVehicle) {
          that.temporaryMarker = marker;
        } else {
          that.tempMarkArray.push(marker);
        }
        // that.allData[key].mark = marker;
        marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
          .subscribe(e => {
            that.liveVehicleName = data.Device_Name;
            // that.drawerHidden = false;
            that.showaddpoibtn = true;
            //that.drawerState = DrawerState.Docked;
            that.onClickShow = true;

          });
        that.getAddress(data.last_location);

      });
    }

  }
}

socketInit(pdata, center = false) {
  let that = this;

  that._io.emit('acc', pdata.Device_ID);
  that.socketChnl.push(pdata.Device_ID + 'acc');
  that._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {

    if (d4 != undefined)

      // console.log('d4 data: ', d4);
      (function (data) {

        if (data == undefined) {
          return;
        }

        if (that.selectedVehicle !== undefined) {
          var temp123;
          if (that.temporaryMarker) {
            that.temporaryMarker.remove();
            temp123 = "cleared";
          }
          that.temporaryInterval = setInterval(() => {
            if (that.temporaryMarker) {
              that.temporaryMarker.remove();
              clearInterval(that.temporaryInterval);
              console.log("temporaryInterval cleared!!")
            } else {
              if (temp123 === 'cleared') {
                if (that.temporaryInterval) {
                  clearInterval(that.temporaryInterval);
                }
              }
            }
          }, 10);
        } else {
          // var temp122;
          if (that.tempMarkArray.length > 0) {
            for (var t = 0; t < that.tempMarkArray.length; t++) {
              that.tempMarkArray[t].remove();
            }
          }
        }
        console.log("on ping: ", data)
        if (data._id != undefined && data.last_location != undefined) {
          var key = data._id;
          that.impkey = data._id;
          that.deviceDeatils = data;
          let ic = _.cloneDeep(that.icons[data.iconType]);
          if (!ic) {
            return;
          }
          ic.path = null;
          if (data.status.toLowerCase() === 'running' && data.last_speed > 0) {
            if (that.plt.is('ios')) {
              ic.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
            } else if (that.plt.is('android')) {
              ic.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
            }
          } else {
            if (data.status.toLowerCase() === 'running' && data.last_speed === 0) {
              if (that.plt.is('ios')) {
                ic.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
              } else if (that.plt.is('android')) {
                ic.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
              }
            } else {
              if (data.status.toLowerCase() === 'idling' && data.last_speed === 0) {
                if (that.plt.is('ios')) {
                  ic.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                } else if (that.plt.is('android')) {
                  ic.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
              } else {
                if (data.status.toLowerCase() === 'idling' && data.last_speed > 0) {
                  if (that.plt.is('ios')) {
                    ic.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
                  } else if (that.plt.is('android')) {
                    ic.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
                  }
                } else {
                  if (that.plt.is('ios')) {
                    ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                  } else if (that.plt.is('android')) {
                    ic.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                  }
                }
              }
            }
          }
          that.otherValues(data);

          if (data.last_location != undefined) {
            that.getAddress(data.last_location);
          }
          var strStr;
          if (that.allData[key]) {
            strStr = ' Address: ' + that.address + '\n Last Updated: ' + moment(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
            that.socketSwitch[key] = data;
            if (that.allData[key].mark != undefined) {
              // that.allData[key].mark.setTitle();

              that.allData[key].mark.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
                // alert('clicked');
                setTimeout(() => {
                  that.allData[key].mark.hideInfoWindow();
                }, 2000)
              });
              that.allData[key].mark.setSnippet(strStr);
              that.allData[key].mark.setIcon(ic);
              that.allData[key].mark.setPosition(that.allData[key].poly[1]);
              var temp = _.cloneDeep(that.allData[key].poly[1]);
              that.allData[key].poly[0] = _.cloneDeep(temp);
              that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

              var speed = data.status == "RUNNING" ? data.last_speed : 0;
              // that.getAddress(data.last_location);
              that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
            } else {
              return;
            }
          }
          else {
            that.allData[key] = {};
            that.socketSwitch[key] = data;
            that.allData[key].poly = [];

            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });

            if (data.last_location != undefined) {
              // that.getAddress(data.last_location);
              strStr = ' Address: ' + that.address + '\n Last Updated: ' + moment(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
              // var strStr = data.Device_Name + '\n' + 'Speed:- ' + data.last_speed + '\n' + 'Last Updated:- ' + data.last_ping_on
              that.allData.map.addMarker({
                title: data.Device_Name,
                snippet: strStr,
                position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                icon: {
                  url: ic.url,
                  size: {
                    width: 20,
                    height: 40
                  }
                },
              }).then((marker: Marker) => {

                that.allData[key].mark = marker;

                // if (that.selectedVehicle == undefined) {
                //   marker.showInfoWindow();
                // }

                // if (that.selectedVehicle != undefined) {
                marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                  .subscribe(e => {
                    if (that.selectedVehicle == undefined) {
                      //that.getAddressTitle(marker);
                    } else {
                      that.liveVehicleName = data.Device_Name;
                      // that.drawerHidden = false;
                      that.showaddpoibtn = true;
                      //that.drawerState = DrawerState.Docked;
                      that.onClickShow = true;
                    }
                  });
                // that.getAddress(data.last_location);
                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
              });
              ///////////////
              if (that.selectedVehicle) {
                // that.addCircleWithAnimation(that.allData[data._id].poly[0].lat, that.allData[data._id].poly[0].lng);
              }
              //////////////
            }
          }

        }
      })(d4)
  })
}

// addCircleWithAnimation(lat, lng) {
//   let that = this;
//   var _radius = 500;
//   var rMin = _radius * 1 / 9;
//   var rMax = _radius;
//   var direction = 1;
//   let GOOGLE: ILatLng = {
//     "lat": lat,
//     "lng": lng
//   };

//   let circleOption: CircleOptions = {
//     'center': GOOGLE,
//     'radius': 500,
//     'fillColor': 'rgb(216, 6, 34)',
//     'fillOpacity': 0.6,
//     'strokeColor': '#950417',
//     'strokeOpacity': 1,
//     'strokeWidth': 0.5
//   };
//   // Add circle

//   let circle: Circle = that.allData.map.addCircleSync(circleOption);
//   that.circleObj = circle;

//   that.circleTimer = setInterval(() => {
//     var radius = circle.getRadius();

//     if ((radius > rMax) || (radius < rMin)) {
//       direction *= -1;
//     }
//     var _par = (radius / _radius) - 0.1;
//     var opacity = 0.4 * _par;
//     var colorString = that.RGBAToHexA(216, 6, 34, opacity);
//     circle.setFillColor(colorString);
//     circle.setRadius(radius + direction * 10);
//   }, 30);
// }

RGBAToHexA(r, g, b, a) {
  r = r.toString(16);
  g = g.toString(16);
  b = b.toString(16);
  a = Math.round(a * 255).toString(16);

  if (r.length == 1)
    r = "0" + r;
  if (g.length == 1)
    g = "0" + g;
  if (b.length == 1)
    b = "0" + b;
  if (a.length == 1)
    a = "0" + a;

  return "#" + r + g + b + a;
}

getAddress(coordinates) {
  let that = this;
  if (!coordinates) {
    that.address = 'N/A';
    return;
  }
//   this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
//     .then(res => {
//       var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
//       // that.saveAddressToServer(str, coordinates.lat, coordinates.long);
//       that.address = str;
//     })
}

// saveAddressToServer(address, lat, lng) {
//   let payLoad = {
//     "lat": lat,
//     "long": lng,
//     "address": address
//   }
//   this.apiCall.saveGoogleAddressAPI(payLoad)
//     .subscribe(respData => {
//       console.log("check if address is stored in db or not? ", respData)
//     },
//       err => {
//         console.log("getting err while trying to save the address: ", err);
//       });
// }

// showNearby(key) {
//   // for police stations
//   var url, icurl;
//   if (key === 'police') {
//     if (this.policeButtonColor == '#d80622') {
//       this.policeButtonColor = '#f4f4f4';
//       this.policeColor = '#d80622';
//     } else {
//       if (this.policeButtonColor == '#f4f4f4') {
//         this.policeButtonColor = '#d80622';
//         this.policeColor = '#fff';
//       }
//     }
//     if (this.plt.is('android')) {
//       icurl = './assets/imgs/police-station.png';
//     } else if (this.plt.is('ios')) {
//       icurl = 'www/assets/imgs/police-station.png';
//     }
//     url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=police&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";

//   } else if (key === 'petrol') {
//     if (this.petrolButtonColor == '#d80622') {
//       this.petrolButtonColor = '#f4f4f4';
//       this.petrolColor = '#d80622';
//     } else {
//       if (this.petrolButtonColor == '#f4f4f4') {
//         this.petrolButtonColor = '#d80622';
//         this.petrolColor = '#fff';
//       }
//     }
//     if (this.plt.is('android')) {
//       icurl = './assets/imgs/gas-pump.png';
//     } else if (this.plt.is('ios')) {
//       icurl = 'www/assets/imgs/gas-pump.png';
//     }
//     url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=gas_station&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";

//   }

//   console.log("google maps activity: ", url);
//   this.apiCall.getSOSReportAPI(url).subscribe(resp => {
//     console.log(resp.status);
//     if (resp.status === 'OK') {
//       console.log(resp.results);
//       console.log(resp);
//       var mapData = resp.results.map(function (d) {
//         return { lat: d.geometry.location.lat, lng: d.geometry.location.lng };
//       })

//       let bounds = new LatLngBounds(mapData);

//       for (var t = 0; t < resp.results.length; t++) {
//         this.allData.map.addMarker({
//           title: resp.results[t].name,
//           position: {
//             lat: resp.results[t].geometry.location.lat,
//             lng: resp.results[t].geometry.location.lng
//           },
//           icon: {
//             url: icurl,
//             size: {
//               height: 35,
//               width: 35
//             }
//           }
//         }).then((mark: Marker) => {

//         })
//       }
//       this.allData.map.moveCamera({
//         target: bounds
//       });
//       this.allData.map.setPadding(20, 20, 20, 20);
//       this.zoomLevel = 10;
//     } else if (resp.status === 'ZERO_RESULTS') {
//       if (key === 'police') {
//         this.showToastMsg('No nearby police stations found.');
//       } else if (key === 'petrol') {
//         this.showToastMsg('No nearby petrol pump found.');
//       }
//     } else if (resp.status === 'OVER_QUERY_LIMIT') {
//       this.showToastMsg('Query limit exeed. Please try after some time.');
//     } else if (resp.status === 'REQUEST_DENIED') {
//       this.showToastMsg('Please check your API key.');
//     } else if (resp.status === 'INVALID_REQUEST') {
//       this.showToastMsg('Invalid request. Please try again.');
//     } else if (resp.status === 'UNKNOWN_ERROR') {
//       this.showToastMsg('An unknown error occured. Please try again.');
//     }
//   })
// }

showToastMsg(msg) {
  this.toastCtrl.create({
    message: msg,
    duration: 2000,
    position: 'bottom'
  }).present();
}

reCenterMe() {
  // console.log("getzoom level: " + this.allData.map.getCameraZoom());
  this.allData.map.moveCamera({
    target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
    zoom: this.allData.map.getCameraZoom()
  }).then(() => {

  })
}

// getAddressTitle(marker) {
//   // if(marker)
//   console.log("issue coming from getAddressTitle")
//   var payload = {
//     "lat": marker.getPosition().lat,
//     "long": marker.getPosition().lng,
//     "api_id": "1"
//   }
//   var addr;
//   this.apiCall.getAddressApi(payload)
//     .subscribe((data) => {
//       console.log("got address: " + data.results)
//       if (data.results[2] != undefined || data.results[2] != null) {
//         addr = data.results[2].formatted_address;
//       } else {
//         addr = 'N/A';
//       }
//       marker.setSnippet(addr);
//     })
// }

addCluster(data) {
  var small, large;
  if (this.plt.is('android')) {
    small = "./assets/markercluster/small.png";
    large = "./assets/markercluster/large.png";
  } else if (this.plt.is('ios')) {
    small = "www/assets/markercluster/small.png";
    large = "www/assets/markercluster/large.png";
  }
  let markerCluster: MarkerCluster = this.allData.map.addMarkerClusterSync({
    markers: data,
    icons: [
      {
        min: 3,
        max: 9,
        url: small,
        size: {
          height: 29,
          width: 29
        },
        label: {
          color: "white"
        }
      },
      {
        min: 10,
        url: large,
        size: {
          height: 37,
          width: 37
        },
        label: {
          color: "white"
        }
      }
    ]
  });

  markerCluster.on(GoogleMapsEvent.MARKER_CLICK).subscribe((params) => {
    let marker: Marker = params[1];
    marker.setTitle(marker.get("name"));
    marker.setSnippet(marker.get("address"));
    marker.showInfoWindow();
  });

}

liveTrack(map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {

  var target = 0;

  clearTimeout(that.ongoingGoToPoint[id]);
  clearTimeout(that.ongoingMoveMarker[id]);

  if (center) {
    map.setCameraTarget(coords[0]);
  }

  function _goToPoint() {
    if (target > coords.length)
      return;


    // console.log("issue coming from livetrack 1", mark)
    var lat = 0;
    var lng = 0;
    if (mark.getPosition().lat !== undefined || mark.getPosition().lng !== undefined) {
      lat = mark.getPosition().lat;
      lng = mark.getPosition().lng;
    }


    var step = (speed * 1000 * delay) / 3600000; // in meters
    if (coords[target] == undefined)
      return;

    var dest = new LatLng(coords[target].lat, coords[target].lng);
    var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
    var numStep = distance / step;
    var i = 0;
    var deltaLat = (coords[target].lat - lat) / numStep;
    var deltaLng = (coords[target].lng - lng) / numStep;

    function changeMarker(mark, deg) {
      mark.setRotation(deg);
      // mark.setIcon(icons);
    }
    function _moveMarker() {

      lat += deltaLat;
      lng += deltaLng;
      i += step;
      var head;
      if (i < distance) {
        head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
        head = head < 0 ? (360 + head) : head;
        if ((head != 0) || (head == NaN)) {
          changeMarker(mark, head);
        }
        // if (that.selectedVehicle != undefined) {
        //   map.setCameraTarget(new LatLng(lat, lng));
        // }
        that.latlngCenter = new LatLng(lat, lng);
        mark.setPosition(new LatLng(lat, lng));
        /////////
        // that.circleObj.setCenter(new LatLng(lat, lng))
        ////////
        that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
      }
      else {
        head = Spherical.computeHeading(mark.getPosition(), dest);
        head = head < 0 ? (360 + head) : head;
        if ((head != 0) || (head == NaN)) {
          changeMarker(mark, head);
        }
        // if (that.selectedVehicle != undefined) {
        //   map.setCameraTarget(dest);
        // }
        that.latlngCenter = dest;
        mark.setPosition(dest);
        /////////
        // that.circleObj.setCenter(new LatLng(dest.lat, dest.lng))
        ////////
        target++;
        that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
      }
    }
    _moveMarker();
  }
  _goToPoint();
}

// temp1(data){
//   this.ngOnDestroy();
// }

temp(data) {
  // debugger
  console.log("check selected vehicle: ", this.selectedVehicle)
  console.log("check temp function data: ", data)
  if (data.status == 'Expired') {
    let profileModal = this.modalCtrl.create('ExpiredPage', {
      param: data
    });
    profileModal.present();

    profileModal.onDidDismiss(() => {
      this.selectedVehicle = undefined;
    });

  } else {
    let that = this;

    that.data = data;
    that.liveDataShare = data;
    // that.drawerHidden = true;
    //that.drawerState = DrawerState.Bottom;
    that.onClickShow = false;

    if (that.allData.map != undefined) {
      // that.allData.map.clear();
      that.allData.map.remove();
    }

    // console.log("on select change data=> " + JSON.stringify(data));
    debugger
    for (var i = 0; i < that.socketChnl.length; i++)
      that._io.removeAllListeners(that.socketChnl[i]);
    that.allData = {};
    that.socketChnl = [];
    that.socketSwitch = {};
    if (data) {
      if (data.last_location) {

        let mapOptions = {
          // backgroundColor: 'white',
          controls: {
            compass: true,
            zoom: false,
            mapToolbar: true,
            myLocation: true,
            myLocationButton: false,
          },
          gestures: {
            rotate: false,
            tilt: false
          },
          mapType: that.mapKey
        }
        let map = GoogleMaps.create('map_canvas', mapOptions);
        map.animateCamera({
          target: { lat: 20.5937, lng: 78.9629 },
          zoom: 15,
          duration: 1000,
          padding: 0  // default = 20px
        });

        map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
        // map.moveCamera({
        //   target: {
        //     lat: data.last_location['lat'], lng: data.last_location['long']
        //   }
        // });
        map.setPadding(20, 20, 20, 20);
        // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
        that.allData.map = map;
        // that.weDidntGetPing(data);

        that.socketInit(data);
      } else {
        that.allData.map = that.newMap();
        that.socketInit(data);
      }

      if (that.selectedVehicle != undefined) {
        // that.drawerHidden = false;
        //that.drawerState = DrawerState.Docked;
        that.onClickShow = true;
      }
    }
    that.showBtn = true;
  }
}
info(mark, cb) {
  mark.addListener('click', cb);
}

setDocHeight() {
  let that = this;
  that.distanceTop = 200;
  //that.drawerState = DrawerState.Top;
}

}
