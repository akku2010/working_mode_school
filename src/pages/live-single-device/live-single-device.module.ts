import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiveSingleDevice } from './live-single-device';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';
import { CallNumber } from '@ionic-native/call-number';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    LiveSingleDevice,
  ],
  imports: [
    IonicPageModule.forChild(LiveSingleDevice),
    IonBottomDrawerModule,
    SelectSearchableModule
  ],
  providers: [CallNumber]
})
export class LiveSingleDeviceModule {}