import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
declare var RazorpayCheckout: any;
@IonicPage()
@Component({
  selector: 'page-fee',
  templateUrl: 'fee.html',
})
export class FeePage {
  index: number = 0;
  pendingList: any = [
    {
      id: this.index + 1,
      title: "January",
      date: new Date().toISOString(),
      status: "Paid",
      amount: "1200"
    },
    {
      id: this.index + 1,
      title: "February",
      date: new Date().toISOString(),
      status: "Paid",
      amount: "1200"
    },
    {
      id: this.index + 1,
      title: "March",
      date: new Date().toISOString(),
      status: "Paid",
      amount: "1200"
    },
    {
      id: this.index + 1,
      title: "April",
      date: null,
      status: "Pending",
      amount: null
    }
  ];
  islogin: any;
  liststudent: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiCallerProvider
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeePage');
    this.onChangedSelect()
  }


  razor_key: string = 'rzp_live_jB4onRx1BUUvxt';
  paymentAmount: number = 120000;
  currency: any = 'INR';
  paynow(item) {
    // payWithRazor() {
    var options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/GO0jiDP.jpg',
      currency: this.currency, // your 3 letter currency code
      key: this.razor_key, // your Key Id from Razorpay dashboard
      amount: item.amount + "00", // Payment amount in smallest denomiation e.g. cents for USD
      name: "xyz",
      prefill: {
        email: this.islogin.email,
        contact: this.islogin.phn,
        name: this.islogin.fn + ' ' + this.islogin.ln
      },
      theme: {
        color: '#ef4138'
      },
      modal: {
        ondismiss: function () {
          console.log('dismissed')
        }
      }
    };

    var successCallback = function (payment_id) {
      alert('payment_id: ' + payment_id);
      this.updateExpDate();
    };

    var cancelCallback = function (error) {
      alert(error.description + ' (Error ' + error.code + ')');
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
    // }
  }
  // getStudent() {
  //   var data =
  //   {
  //     "_find": { "parent": { "_eval": "Id", "value": this.islogin._id } }
  //   }

  //   this.apiCall.startLoading().present();
  //   this.apiCall.getStudent(data)
  //     .subscribe(data => {
  //       console.log("student data", data);
  //       // this.portstemp = data;
  //       this.liststudent = [];
  //       for (var i = 0; i < data.length; i++) {
  //         this.liststudent.push({ 'name': data[i].name.fname, 'id': data[i]._id, 'scheduleId': data[i].scheduledTrips })

  //       }
  //       console.log("student data name", this.liststudent);
  //       this.apiCall.stopLoading();

  //       localStorage.setItem('student', data);
  //       // this.isstudent = localStorage.getItem('student');
  //     },
  //       error => {
  //         this.apiCall.stopLoading();
  //         console.log(error);
  //       });
  // }

  onChangedSelect() {
    // console.log("selected student: ", selectedStudent);
    this.pendingList = [];
    var url = this.apiCall.link + "users/getTransportFee?parent=" + this.islogin._id;
    this.apiCall.startLoading().present();
    this.apiCall.getUrl(url)
      .subscribe((data) => {
        this.apiCall.stopLoading();
        console.log("response data: ", data);
        this.pendingList = data;
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        })

  }
}
