import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeePage } from './fee';
// import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    FeePage,
  ],
  imports: [
    IonicPageModule.forChild(FeePage),
    // SelectSearchableModule
  ],
})
export class FeePageModule {}
